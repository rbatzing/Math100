Fundamental Math
========================================================
title: Lecture notes: Robert Batzinger
author: IT100 Session 4: Algebra Review and Functions
date: 20 Sept 2017 presented by Dr Bob Batzinger
autosize: true
css: mathclass.css
font-import: http://fonts.google.com/css?family=Bree Serif
font-family: 'Bree Serif'


====================================
type: section

# Agenda

* Simplification of relationships
* Basic Algebraic strategies
* Functions vs Non-Functions
* Transformation of functions

====================================
type: section

## 1. Simplification

Some useful relationships
==========================
$$\huge\begin{array}{rcl}
(x+a)(x+b) &=& x^2 + (a+b)x + ab\\
(x+a)(x+a) &=& x^2 + 2a + a^2\\
(x+a)(x-a) &=& x^2 - a^2\\
(ax+b)(cx+d)&=& ac x^2 + (ad+bc)x + bd\\
\end{array}$$


Simplifying  using additive inverses
===================================
$$\huge\begin{array}{rcl}
x + 3 & = & 5 \\
x = x + 3 -(3) & = & 5 - (3) = 2 \\
& & \\
x + 7 & = & 2x + 3 \\
7 = x + 7 - (x)  & = & 2x + 3 - (x) = x + 3\\
4 = 7 - (3) & = & x + 3 - (3) = x \\
\end{array}$$


Simplifying using reciprocal values
===================================
  
$$\huge\begin{array}{rcl}
2 x & = & 16\\
{2x \over (2)} & = & {16 \over (2)}\\
x & = & 8 \\
\end{array}$$

***

$$\huge\begin{array}{rcl}
{3\over x} &= & 36\\
1 = {3(x)\over (3)x} &= & {36(x) \over (3)} = 12x\\
{1\over 12} = {1\over (12)} &=& {12x\over (12)} = x\\
\end{array}$$

Case 1
================
$$\huge\begin{array}{c}
\left({10h^2 - 9h - 9\over 2h^2 - 19h + 24}\right)\left({h^2 - 16h + 64\over 5h^2 - 37h - 24}\right)\\
{(5h+3)(2h-3)(h-8)(h-8)\over (2h-3)(h-8)(5h+3)(h-8)}\\
{(5h+3)(2h-3)(h-8)(h-8)\over (5h+3)(2h-3)(h-8)(h-8)}\\
1\\
\end{array}$$


Case 2
================
$$\huge\begin{array}{c}
\left({x^2 - x - 6\over x^2 + x - 6}\right) \left({x^2 + 8x + 15\over
x^2 - 9}\right)\\
{(x-3)(x+2)(x+3)(x+5)\over (x+3)(x-2)(x+3)(x-3)}\\
{(x+2)(x+5)\over (x-2)(x+3)}= {x^2 +7x + 10\over x^2 + x - 6}\\
\end{array}$$

Challenge
========================

Simplify the following:   
$$\huge\begin{array}{c}
\left({y^2 + 10y + 25\over y^2 + 11y + 30}\right)\\
& & \\
\left({2x^2 + 7x - 4\over
4x^2 + 2x - 2}\right)\\
& & \\
\left({c^2 + 2c - 24\over c^2 + 12c + 36}\right)\left({c^2 - 10c + 24\over c^2 - 8c + 16}\right)\\
\end{array}
$$

====================================
type: section

## 2. Algebraic Strategies


Algebra 
=======================

* from Arabic "al-jabr"    
meaning **"reunion of broken parts"**

* a systematic application of the basic rules to simplify and solve equations


Strategy for simultaneous equations
=======================

1. Remove common multiples and simplify the equations separately
2. Transform the equations placing variables on one side of the equals and constants on the other
3. Combine terms
4. Solve for one variable
5. Substitute the value of the known variable to find the values of the others


Case 1
=============================

$$\huge\begin{array}{rcl}
3x - 2y = 57; & & x + y  = 44\\
& & \\
2x + 2y=(2)(x + y)  &=& (2)\times 44 = 88\\
& & \\
5x = (3x-2y)+ 2x +2y &= & 57 + 88 = 145\\
x=5x/5&=&145 /5 = 29\\
& & \\
29 + y &=& 44\\
y =29 + y -(29) & = & 44- (29)=  15\\
\end{array}$$



Case 2
=============================

$$\huge\begin{array}{rcl}
8x - 3y = 8; & & x + 2y  = 20\\
& & \\
8x +16y = 8(x + 2y) &=& 8\times 20=160\\
19y = 8x +16y -(8x - 3y) &=& 160 - 8 = 152\\
y = 19y/19 & = & 155/19 = 8\\
& & \\
x + 16 =x +2 \times 8 &= & 20\\
x = x + 16 - (16) &=& 20 -(16) = 4\\
\end{array}$$ 



Challenge
=================================
$$\huge\begin{array}{rcl}
3x + 7 & = & 5x - 3\\
& & \\
{-2 \over (3x+5)} & = & 6x - 4\\
& & \\
{2x + 7\over x - 2} &=& 27\\
& & \\
2x+5& = & 6y -15\\
x+8& = & y + 6\\  
\end{array}$$

====================================
type: section

## 3. Functions vs Non-Functions


Function Definition
==========================

<large>
A function is a relation in which each possible input value leads to exactly one output value.
</large>

$$\huge\begin{eqnarray}
 Input & \rightarrow & \hbox{Function} & \rightarrow & Output\\
x\qquad & & f(x) & & {}\qquad y\\
\hbox{domain} & & y = {x \over 5} & & \hbox{range}\\
\large [0, 1, 2 ... n] & &  & & \large [0, 0.2, 0.4 ... n/5] \\
\end{eqnarray}$$


Common non-function relationships
=====================
**Circle**   
$\huge 25= x^2 + y^2$    
![circle](img/grcircle.png)

***

**Ellipse**   
$\huge 25 = 2x^2+ 5y^2$   
![circle](img/grellipse.png)

Hyperbola
============

$\huge 25= x^2 y^2$    
![circle](img/grhyper.png)

***

$\huge 25=\left(x+1\right) \left(x-5\right) \left(y-1\right) \left(y+4\right)$    
![circle](img/grhyper2.png)


Egg and Heart Shapes
============================

$\huge 25 = \frac{x^2}{0.5} + \frac{5y^2}{1-0.1x}$    
![egg](img/gregg.png)

***

$\huge\left(x^2+y^2-2ax\right)^2 = 4a^2\left(x^2+y^2\right)$     ![flower](img/grcartoid.png)


Flowers
==========================

$\huge\left(x^2+y^2\right)^3 = 4x^2y^2$    
![flower](img/gr4petal.png) 


Challenge
==========================

* Determine why these are considered non-functions in this form?

* Is there a domain or mathematical transform that would make any of these a function? 

* Answer the questions on pg 20: Numbers 52-69
* Give answers for Applied Exercises on pg 21: Numbers 88-90

====================================
type: section

## 4. Inverse Functions

<large>
An inverse function will convert output back to the corresponding input value.</large>

$$\huge\begin{eqnarray}
 Input & \rightarrow & \hbox{Function} & \rightarrow & Output\\
x\qquad & & f(x) & & {}\qquad y\\
& & & & \\
Output  & \rightarrow & \hbox{Inverse Function} & \rightarrow & Input\\
y\qquad & & f\prime(y) & & {}\qquad x\\
\end{eqnarray}$$



Challenge
===============

| Input $\huge(x)$ | Function | Output $\huge (y)$ | Inverse Function| Reverted $\huge (x_2)$ | Does $\huge x = x_2$ ?|
|--------|-------|--------|----------|--------|---|
|$\Large [-5,-2,0,2,5]$ | $\huge y = 3x$ | $\Large [-15,-6,0,6,15]$ |  $\huge x_2 = y / 3$ |$\Large [-5,-2,0,2,5]$ | True |
|$\Large [-5,-2,0,2,5]$ | $\huge y = x^2$ | $\Large [25,4,0,4,25]$ |  $\huge x_2 = \sqrt{y}$ | | |
|$\Large [-5,-2,0,2,5]$ | $\huge y = 3x + 4$ | $\Large [-11,-2,4,10,19]$ |  | | |
|$\Large [-5,-2,0,2,5]$ | $\huge y = 1/x$ |  |  | | |
|$\Large [-5,-2,0,2,5]$ | $\huge y = |x|/x$ |  |  | | |


====================================




type: section

## 5. Function Transforms




Function transforms
===========

| Transform| Formula|Parabola | Line | Normal Curve |
|-------------|-------|----|----------|---------|
| (N) None |$\huge y  =  f(x)$ | $\huge y = x^2$ | $\huge y = 2x$ | $\Huge y = {1\over \sigma\sqrt{2\pi}} e^{-{(x-\mu)^2\over 2 \sigma^2)}}$ | 
| (A) Horizontal |$\huge y  =  f(x+A)$ |$\huge y = (x+A)^2$ | $\huge y = 2(x + A)$ | $\Huge y = {1\over \sigma\sqrt{2\pi}} e^{-{(x+A-\mu)^2\over 2 \sigma^2)}}$ |
| (B) Vertical |$\huge y  =  f(x) + B$ | $\huge y = x^2 + B$ |  $\huge y = 2x + B$ |$\Huge y = B+ {1\over \sigma\sqrt{2\pi}} e^{-{(x-\mu)^2\over 2 \sigma^2)}}$ |
| (C) Amplitude | $\huge y  =  C\times f(x)$ | $\huge y = C x^2$ |  $\huge y = C(2x)$ |$\Huge y = {C\over \sigma\sqrt{2\pi}} e^{-{(x-\mu)^2\over 2 \sigma^2)}}$ |

Graph of Transformations
=======================
![transforms](img/grfunct.png)

***
![transforms](img/grlinear.png)


Normal Graph
=====================
![transforms](img/grnorm.png)

Challenge
=======================

* Compare the graph of $\huge f(x+A)$ to that of $\huge f(x)$

* Compare the graph of $\huge f(x) + B$  to that of $\huge f(x)$

* Compare the graph of $\huge f(x)\times C$ to that of $\huge f(x)$ 


6. Topic for Session 5
==============================

**Properties of Linear functions**

Read  Precalculus Chapter 2
 
