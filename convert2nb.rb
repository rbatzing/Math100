puts "Name of the file to convert:"
STDOUT.flush
infname = gets.chomp


puts "Name of the converted file:"
STDOUT.flush
outfname = gets.chomp

puts "\nConverting #{infname} -> #{outfname}"
fin = File.open(infname,"r")
text = fin.readlines

fout = File.open(outfname,"w")
fout.puts <<EOM
---
title: 'IT100 Session 2: - Functions'
author: 'Answers key: Robert Batzinger'
output:
  html_notebook:
    autosize: yes
    self_contained: yes
    toc: yes
    toc_depth: 3
    toc_float: yes

  pdf_document:
    toc: yes
    toc_depth: 3
---
EOM

printing = 0
count = 0
while count < text.length
   	if text[count] =~ /^[\-\*\=]{3,}\s*$/
    	if text[count+1]  =~ /^type: section/
        	count = count + 2
        	while text[count] =~ /^\s*$/
        		count = count + 1
        	end
       		text[count].sub!(/^\#*\s*/, "\# ")
       	else
       		count = count + 1
       	end
    end
    if text[count] !~ /^\s*$/
    	if text[count + 1] =~ /^\={3,}\s*$/
    		text[count].sub!(/^\#*\s*/, "\#\# ")
    	end
    end
    text[count].gsub!(/\\huge/i,"\\large")
    fout.puts text[count]
    count = count + 1
end
