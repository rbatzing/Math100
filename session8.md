Fundamental Math
========================================================
title: Lecture notes: Robert Batzinger
author: IT100 Session 8: Trends and seasonal effects
date: 5 Sept 2017
autosize: true
css: mathclass.css
font-import: http://fonts.google.com/css?family=Bree Serif
font-family: 'Bree Serif'


====================================
type: section

# Agenda

Agenda
==========================
1. Introduction to Forecasting
2. Linear, exponential, logrithmic trends
3. Seasonal effects
4. Measuring Accuracy of Forecasting 

====================================
type: section

## 1. Introduction to Forecasting


Forecasting
===================

* Uses mathematical models:
  * to determine general trends in historical data
  * to project possible futures based on trends discovered
  * to monitor variations from expected trends
  * uses statistics to determine the level of confidence

* Assumes that the content does not change
  * Subject to changes in technology, environment, politics




Drug Pharmacokinetics: Indomethacin
====================================
![plot of chunk unnamed-chunk-1](session8-figure/unnamed-chunk-1-1.png)
***
![plot of chunk unnamed-chunk-2](session8-figure/unnamed-chunk-2-1.png)

Orange Tree Growth
========================
![plot of chunk unnamed-chunk-3](session8-figure/unnamed-chunk-3-1.png)
***
![plot of chunk unnamed-chunk-4](session8-figure/unnamed-chunk-4-1.png)


===================================
type: section

## 2. Determining trends

Regression
================

$$ b = \bar y - m \bar x $$
$$ m = { \sum_{i=1}^n   (x_i -\bar x)(y_i - \bar y)\over
    \sum_{i=1}^n   (x_i -\bar x)^2 }$$



Excel Trendlines
==================


Population Modelling
==========================
![plot of chunk unnamed-chunk-5](session8-figure/unnamed-chunk-5-1.png)

Challenge
==================
left: 40%

Use the Down Jones Industrial Average

* Determine the best trendline for this dataset
* Use the trendline to determine to recalculate the dataset
* Determine the difference between the forecast 
* Use the trendline to determine the subsequent 6 periods


====================================
type: section

## 3. Seasonal effects


Air Passengers 
=================
![plot of chunk unnamed-chunk-6](session8-figure/unnamed-chunk-6-1.png)
***

![plot of chunk unnamed-chunk-7](session8-figure/unnamed-chunk-7-1.png)

Trend
=================

![plot of chunk unnamed-chunk-8](session8-figure/unnamed-chunk-8-1.png)


Steps of Seasonal models
=========================

1. Determine the annual totals
2. Establish the year to year trends
3. Calculate a projections  to the next 5 years
4. Determine the fraction of the year for each season
5. Calculate the average seasonal fraction for the dataset
6. Apply the seasonal fraction to the annual projection
7. Recalculate the seasonal data based on annual projects.


UK Gas Consumption Data
====================
left: 66%
$$\begin{array}{lrrrrr}
 Year  &  Qtr1  &  Qtr2  &  Qtr3  &  Qtr4 & Total\\
1972   & 317.0   & 230.5   & 152.1   & 336.2 & 1035.8\\
1973   & 371.4   & 240.1   & 158.5   & 355.4 & 1125.4\\
1974   & 449.9   & 286.6   & 179.3   & 403.4 & 1319.2\\
1975   & 491.5   & 321.8   & 177.7   & 409.8 & 1400.8\\
1976   & 593.9   & 329.8   & 176.1   & 483.5 & 1583.3\\
1977   & 584.3   & 395.4   & 187.3   & 485.1 & 1652.1\\
1978   & 669.2   & 421.0   & 216.1   & 509.1 & 1815.4\\
1979   & 827.7   & 467.5   & 209.7   & 542.7 & 2047.6\\
1980   & 840.5   & 414.6   & 217.7   & 670.8 & 2143.6\\
1981   & 848.5   & 437.0   & 209.7   & 701.2 & 2196.4\\
1982   & 925.3   & 443.4   & 214.5   & 683.6 & 2266.8\\
1983   & 917.3   & 515.5   & 224.1   & 694.8 & 2351.7\\
1984   & 989.4   & 477.1   & 233.7   & 730.0 & 2430.2\\
1985  & 1087.0   & 534.7   & 281.8   & 787.6& 2691.1\\
1986  & 1163.9   & 613.1   & 347.4   & 782.8& 2907.2\\
\end{array}$$

***

![plot of chunk unnamed-chunk-9](session8-figure/unnamed-chunk-9-1.png)

Stage
===============

![plot of chunk unnamed-chunk-10](session8-figure/unnamed-chunk-10-1.png)


Seasonal effects as a fraction
=================================
left:66%

$$\begin{array}{lrrrr}
Yr & Q1 & Q2 & Q3 & Q4\\
1972 & 0.306043638 & 0.222533308 & 0.146843020 & 0.324580035\\
1973 & 0.330015994 & 0.213346366 & 0.140838813 & 0.315798827\\
1974 & 0.341040024 & 0.217252881 & 0.135915706 & 0.305791389\\
1975 & 0.350870931 & 0.229725871 & 0.126856082 & 0.292547116\\
1976 & 0.375102634 & 0.208299122 & 0.111223394 & 0.305374850\\
1977 & 0.353671085 & 0.239331760 & 0.113370861 & 0.293626294\\
1978 & 0.368623995 & 0.231904814 & 0.119037127 & 0.280434064\\
1979 & 0.404229342 & 0.228316077 & 0.102412581 & 0.265042000\\
1980 & 0.392097406 & 0.193412950 & 0.101558127 & 0.312931517\\
1981 & 0.386313968 & 0.198961938 & 0.095474413 & 0.319249681\\
1982 & 0.408196577 & 0.195606141 & 0.094626787 & 0.301570496\\
1983 & 0.390058256 & 0.219203130 & 0.095292767 & 0.295445848\\
1984 & 0.407126985 & 0.196321290 & 0.096164925 & 0.300386799\\
1985 & 0.403924046 & 0.198691985 & 0.104715544 & 0.292668426\\
1986 & 0.400350853 & 0.210890204 & 0.119496423 & 0.269262521\\
Mean & 0.374511049 & 0.213586522 & 0.113588438 & 0.298313991\\
\end{array}$$


Seasonal effects
==================================

![plot of chunk unnamed-chunk-11](session8-figure/unnamed-chunk-11-1.png)


Applying seasonal effects to the trend
============================

![plot of chunk unnamed-chunk-12](session8-figure/unnamed-chunk-12-1.png)


====================================
type: section

## 4. Measuring Accuracy of Forecasting 


Measuring goodness of fit
=================================
left:60%

Correlation coefficent 
* A measure of the linear relationship
* 1 if Positive correlation
* -1 if Negative correlation


$\large R^2$
* Determine if the error pattern follows the curve
* Suggests the fraction of the behavior explained by the model

***

Analysis of Variance
* A measure of the trends in the pattern of errors
* Determines if the model explains the pattern
 
MEAN ABSOLUTE DEVIATION
=========================
$$\large MAD = \sum_{i=1}^n {|obs_i - est_i| \over n} $$

* differences of observed and estimate value
* number of measurements

***

| Year | Obs   |Predict   |  Abs Dev |
|-----|-----|------|----|
| 1972 |1035.8 |1048.109 | 12.309167|
| 1973 |1125.4 |1174.252|  48.851667|
| 1974 |1319.2 |1300.394|  18.805833|
| 1975 |1400.8 |1426.537|  25.736667|
| 1976 |1583.3 |1552.679|  30.620833|
| 1977 |1652.1 |1678.822|  26.721667|
| 1978 |1815.4 |1804.964| 10.435833|
| 1979 |2047.6 |1931.107| 116.493333|
| 1980 |2143.6 |2057.249|  86.350833|
| 1981 |2196.4 |2183.392|  13.008333|
| 1982 |2266.8 |2309.534|  42.734167|
| 1983 |2351.7 |2435.677|  83.976667|
| 1984 |2430.2 |2561.819| 131.619167|
| 1985 |2691.1 |2687.962|   3.138333|
| 1986 |2907.2 |2814.104|  93.095833|



Growth of Chicks
==================

![plot of chunk unnamed-chunk-13](session8-figure/unnamed-chunk-13-1.png)
***
|Model | $\large R^2$ | $MAD$ |
|-----|----|----|
|Linear Model:  | 0.9855 |5.106 |
|Exponental Model:  | 0.9899 | 5.074 |


Challenge
==========

Use the Air Passenger Data
* Determine the annual number of passengers
* Use the annual figures to determine the best trendline
* Use the trendline to determine the MAD and the next 5 years
* Determine the seasonal effects
* Apply the seasonal adjustment and determine the new MAD
* Apply the seasonal adjustments to the forecast
