Some thoughts on learning
========================================================
author: Ajarn Bob Batzinger
date:  6 Sept 2017
autosize: true
css: mathclass.css
font-import: http://fonts.google.com/css?family=Bree Serif
font-family: 'Bree Serif'

The Elephant in the Room
======================
left:25%
incremental: true


![Blue Elephant](img/blueelephant.png)

***

**Feelings about 3 core skills**

* Communicating in English
* Understanding Mathematics
* Programming Computers


Rudyard Kipling
=======================
I Keep Six Honest Serving Men    
from **The Elephant's Child** (1902)

    I keep six honest serving-men
      (They taught me all I knew);
    Their names are What and Why and When 
      And How and Where and Who.

==================================
incremental:true 

    I send them over land and sea,
      I send them east and west;
    But after they have worked for me,
      I give them all a rest.

<brk>

    I let them rest from nine till five,
      For I am busy then,
    As well as breakfast, lunch, and tea,
      For they are hungry men.


=======================================
incremental:true 

    But different folk have different views; 
      I know a person small-
    She keeps ten million serving-men,
      Who get no rest at all!

<brk>

    She sends'em abroad on her own affairs,
      From the second she opens her eyes-
    One million Hows, two million Wheres,
      And seven million Whys!

Applied to Functions
====================
incremental: true
left: 40%

![Neurons](img/quadrocopter.png)

[Quadrocopters](https://www.youtube.com/watch?v=4ErEBkj_3PY)
1:46-8:00


***

| Level | Relationship |
|---------------|-----------------------|
| Position:| $\huge x$ |
| Distance:| $\huge d = \Delta x = \huge x_1 - x_2$ |
| Speed:|  $\huge s = d / t = (\Delta x / \Delta t)$ |
| Acceleration:| $\huge a =  (\Delta x / \Delta t)$ |
| Jerk:|$\huge j =  (\Delta a / \Delta t)$  |
| Snap:| $\huge k =  (\Delta j / \Delta t)$ |


Balloon
======================
incremental: true

![Balloon](img/balloon.png)

***

$$\Huge PV = nRT$$

* Pressure
* Volume
* Number of atoms
* Rate constant
* Temperature


Brain Development
======================
left: 50%
incremental: true

![child](img/dawn8.jpg)

***
![Neurons](img/neurons.png)


[Rat Brain cell control](https://www.youtube.com/watch?v=1-0eZytv6Qk)

Brain Plasticity
=================
**Enhancers**

* Goals and rewards
* Repeated exposure
* Multiple inputs
* Feedback through testing
* Encouragement
* Sleep

***
**Inhibitors**

* Distractions
* Inconsistencies
* Fear and distrust
* Boredom and weariness
* Loneliness
* Illness
