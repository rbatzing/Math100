Fundamental Math
========================================================
title: Lecture notes: Robert Batzinger
author: IT100 Session 2: Numbers - Answers
date: 5 Sept 2017
autosize: true
css: mathclass.css
font-import: http://fonts.google.com/css?family=Bree Serif
font-family: 'Bree Serif'


====================================
type: section

# Topic for the Day: 

## Numbers


Agenda
====================================
left: 40%
* Integers
  * Counting
  * Factoring
  * Number systems
  
***   
* Real Numbers
  * Rational numbers
  * Irrational number
  * Imaginary numbers
 
* Coordinate space

====================================
type: section

## Unit 1: COUNTING

Counting
===================
left: 20%

![counting](img/fruit.png)

***

* How would you count all this fruit?
```{txt}
* By the number of fruit of each type
* By common units of weight, cost or sale value
```   
* How would you represent the quantity of all 
this fruit with a single meaningful number?
```{txt}
* Only in a common unit for all: weight, cost or sale value
```   
Counting on fingers
=====================
left: 20%

![counting](img/counting.png)

***

* Identify major ways to count with fingers
```{txt}
* one to one representation extended to both hands
* finger order tp display 10 units on one hand
* one to one on working hand; group count on the other
```
* Do all cultures count with their hands the same way?
` no `
* What is the maximum number that can represented by fingers?
` 144`

====================================
type: section

## Unit 2: COUNTING SYSTEMS

Tally
=====================
left:20%

![tally](img/tally.png)
![tallies](img/tallies.png)

***
* How do this system work? ` 1 to 1 ticks grouped in units of 5`
* How do you add and substract in this system? ` add or erase tick mark correspondingly`
* What is the key disadvantage of this representation? 
* How do you add and substract in this system? ` run out of space with large number counts`



Roman Numerals
=========================
left: 20%

    MCMXLVII
    DCCCLXXX
    MMCMXCIX
***

* What do all the letters mean?
* What was the maximum number that could be represented?
* How do you add and substract in this system?
* Is this system of counting still used?
 
Decimal numbers
=========================
left: 60%
 
    2 x 100 ------- 200
    |   4 x 10 ----  40
    |   |   3 x 1 -   3
    |   |   |         
    2   4   3       243

***

* Why is this more efficient than tally?
* What is the role of the O numeral?
* What do Arabic numerals actually look like?


Addition and subtraction
=======================================

* How does addition and subtraction work?
* How does multi-column addition work?

***
     101 + 93 =
     23 + 5741 =
     35 + 17 =
     783 + 298 =


====================================
type: section

## Unit 3: NUMBER USING A DIFFERENT BASE


Binary numbers
======================
left: 66%

     1 x 32 -------------- 32
     |  0 x 16 ------------ 0
     |  |  1 x 8 ---------- 8
     |  |  |  1 x 4 ------- 4
     |  |  |  |  0 x 2 ---- 0
     |  |  |  |  |  1 x 1 - 1
     |  |  |  |  |  |        
     1  0  1  1  0  1      45

***
Base: 2   

Range of digits:   
0, 1

Values of columns:    
1, 2, 4, 8, 16, 32, ...


Binary addition
===================

* Determine the decimal equivalents.
* Figure out the binary sums of these binary numbers

***

        1010 + 111 =
        
        10111 + 101 =


Octal numbers
=====================
left: 66%

     2 x 512 -------- 1024
     |  7 x 64 ------  448
     |  |  1 x 8 ----    8
     |  |  |  5 x 1 -    5
     |  |  |  |           
     2  7  1  5       1485

***
Base: 8   

Range of digits:   
0, 1, 2, 3,
4, 5, 6, 7 

Values of columns:    
1, 8, 64, 512, 4096, ...


Hexadecimal numbers
======================
left: 60%

     10 x 4096 ------- 40960
     |  0 x 256 ------     0
     |  |  14 x 16 ---   224
     |  |  |  15 x 1 -    15
     |  |  |  |  
     A  0  E  F        41199

***
Base: 16   

Range of digits:   
0, 1, 2, 3, 4, 5, 6, 7,   
8, 9, A, B, C, D, E, F

Values of columns:    
1, 16, 256, 4096, ...

Switching between bases
=======================
left: 50%

* Complete this table

| Decimal | Binary | Octal | Hexa- decimal |
|---------|--------|-------|-------------|
|   39    |        |       |             |
|         | 101010 |       |             |
|         |        |  31   |             |
|         |        |       |     1A      |

***

* Describe the steps to convert numbering systems

* Describe how to do hexadecimal addition

        1AF + 2FF =
        35 + A5 =
       
===============================
type: section

## Unit 4: Alternative number systems

1952 Myanmar Coinage
===================

**Old Kyat**   
* 2pe = mu
* 2mu = mat 
* 5 mat = 1 silver Kyat
* 16 silver kyat = 1 gold kyat

***

**New Kyat**
* 100 pe = 1 kyat
* coins: 5,10,25,50 pyas

Convert the following:
=============================

* Old Kyat to New Kyat:

     1 gold kyat,  2 silver kyat, 4 mat, 1mu =

* New Kyat to Old Kyat:

     27.40 =
     

Egg shipments: 
=================================
left: 60%

**American packaging:** 
* dozen (12)
* gross (144)
* 12-gross (1728)

***

**Thai packaging:**
* 10
* 30
* 300
* 1200

Egg shipments
=============================
left: 60%

America produces 93,162,174,216 eggs each year. If one day’s production is shipped to Thailand what would the size of the shipment be in both standards?

***

* American:

* Thai: 


Urdu counting:
====================

| Urdu Units | Decimal Equivalant | Scientific Notation |
|:-----|-----------:|--------:|
| Hazar | 1,000 | $\huge 10^3$ | 
| Lakh |  100,000 | $\huge 10^5$ |
| Crore | 10,000,000 | $\huge 10^7$ |
| Arab | 1,000,000,000 | $\huge 10^9$ |
| Kharab | 100,000,000,000 | $\huge 10^{11}$ |

Exercise
================================
left: 40%

Determine the size of the following populations
using Urdu counting units:

***

* China: 1,388,336,022

* India: 1,342,512,706

* World: 7,524,885,124

===========================
type: section

## Unit 5: Factoring

Grouping 12 units
============
left: 40%

<tiny><b>0 0 0 0 0 0 0 0 0 0 0 0</b></tiny>     
   
   
<tiny><b>0 0 0 0 0 0</b></tiny>    
<tiny><b>0 0 0 0 0 0</b></tiny>    
   
<tiny><b>0 0 0 0</b></tiny>   
<tiny><b>0 0 0 0</b></tiny>   
<tiny><b>0 0 0 0</b></tiny>   


***

* How many ways can 12 items be evenly grouped?
* When was the concept of a dozen introduced?
* Why do you think the concept of a dozen is so common?


Factoring of numbers
===================
![factors](img/factors.png)


```{txt,echo=FALSE}
$$\Large\begin{array}{cccccclcccccccl}
\small\blacktriangle &\small\circ &[1] & = & 1 \times 1 & = & 1 & &
  & \small  \bullet & [6] & = & 2 \times 3 \times 1 & = & 2 \times 3 \\
\small\blacktriangledown & \small\bullet & [2] & = & 2 \times 1 & = & 2 & & 
\small\blacktriangledown &\small\circ  & [7] & = & 7 \times 1 & = & 7 \\
\small\blacktriangledown &\small\circ &[3] & = & 3 \times 1 & = & 3 & &
& \small\bullet & [8] & = & 2 \times 2 \times 2 \times 1 & = & 2^3 \\
&\small\bullet &[4] & = & 2 \times 2 \times 1 & = & 2^2 & &
 & \small\circ & [9] & = & 3 \times 3 \times 1 & = & 3^2\\
\small\blacktriangledown &\small\circ &[5] & = & 5 \times 1 & = & 5  & & 
&\small\bullet & [10] & = & 2 \times 5 \times 1 & = & 2 \times 5 \\
\end{array}$$
$$\hbox{Key:}\ \ \ \
\blacktriangle\ \hbox{Identity}\ \ \ \ \blacktriangledown\ \hbox{Prime}\ \ \ \
\circ\ \hbox{Odd}\ \ \ \
\bullet\ \hbox{Even}$$
```


Factors
=====================
* Determine the rule for labelling a number as  odd or even.
* Determine a method for determining if a number is prime.
* Identify all the prime numbers between 1 and 25.
* What are the prime factors of these numbers?    
      27, 30, 45, 111, 210  

==========================
type: section

## Unit 6: Real numbers

Real numbers
=======================
Define the following:

* Rational numbers
* Irrational number
* Imaginary numbers

Converting real to integers
============================
left: 30%
* Floor(x) - red
* Round(x) - blue
* Ceiling(x) - green

***

![NumberLines](img/numberline.png)


Converting real to integers
============================
left: 40%

Complete the table using these functions


| Number | round(x) | ceiling(x) | floor(x) |
|--------|----------|------------|----------|
| 1.00 | | | |
| 1.45 | | | |
| 2.99 | | | |
| 3.01 | | | |
| 4.50 | | | |

==========================
type: section

## Unit 7: Coordinate Systems


Cartesian Coordinate space
============================================

![PoundCake](img/plane.jpg)

***

![PoundCake](img/xyz_grid.png)


Polar Coordinates
==============================
![PoundCake](img/polar.png)

***
![PoundCake](img/geographic.gif)


Multiple coordinate space
====================================
![coordinate](img/surface.png)

***

![coordinate](img/einstein-spacetime-grid.jpg)

Dimensions of a cake
====================
left: 60%
* 8 ounce package cream cheese
* 1 1/2 cups butter
* 3 cups white sugar 
* 6 eggs 
* 3 cups all-purpose flour 
* 1 teaspoon vanilla extract

***

![PoundCake](img/cake.png)

A Question
==========================

* What is the maximum number of dimensions possible in a graph on paper?
* How many dimensions are needed to describe variants of this pound cake recipe?
* How would you graph these dimensions on paper?
* How would you record these dimensions in a spreadsheet?

Prepare for next time
==============================

* **Arithmetic operations and properties**
  * Identity
  * Reciprocal
  * Associative
  * Communicative
  * Distributative



