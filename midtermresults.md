
Fundamental Math Unit 1 Test
========================================================
title: Lecture notes: Robert Batzinger
author: IT100 Session 5: Linear Equations
date: 5 Sept 2017
autosize: true
css: mathclass.css
font-import: http://fonts.google.com/css?family=Bree Serif
font-family: 'Bree Serif'

Histogram of Exam results
========================================================
![plot of chunk unnamed-chunk-1](midtermresults-figure/unnamed-chunk-1-1.png)

Time vs Grade
==============

![plot of chunk unnamed-chunk-2](midtermresults-figure/unnamed-chunk-2-1.png)

Parts of the Exam
=================

|Section|Description|Average|Std Dev|
|---|---|---|---|
|Section 1|Discontinous Function|4.1 | 3.9 |
|Section 2|Equation factoring|3.5 | 4.4 |
|Section 3|Function graphing|2.4 | 2.8 | 
|Section 4|Function manipulation|1.0 | 2.8 |
|Section 5|Prime factoring|6.3 | 4.3 |
|Section 6|Number systems|2.3	| 2.2|
|Section 7|Binary,Octal,Hexdecimal|3.3	| 3.7 |
|Section 8|Real to Integer Functions|2.1 | 3.7 |
|Section 9|Precedence|7.6	| 3.3 | 
|Section 10|Exponents|3.7	| 3.4 |
| |Total score|35.5 |22.4 |

