---
title: "R Notebook"
output: html_notebook
---

This is an [R Markdown](http://rmarkdown.rstudio.com) Notebook. When you execute code within the notebook, the results appear beneath the code. 

Try executing this chunk by clicking the *Run* button within the chunk or by placing your cursor inside it and pressing *Ctrl+Shift+Enter*. 

```{r}
scores=c(27,130,55,108,32,95,
43,73,39,26,21,100,100,238,
0,65,51,178,0,153,26,40,
12,102,54,263,290,181,105,26,
117,162,41,100,169,110,157,55,
26,88,132,102,57,214,57,377,30)

hist(scores, breaks=25)
```

```{r}
scores = c(16,57,30,51,18,41,19,35,24,15,
12,43,44,103,7,35,24,78,5,70,15,21,10,46,
29,114,124,81,48,13,55,70,24,46,77,52,67,
27,19,39,61,50,32,91,32,158,16)

hist(scores,breaks=30)

```


Add a new chunk by clicking the *Insert Chunk* button on the toolbar or by pressing *Ctrl+Alt+I*.
```{r}
x = c(0,10,20,25,30,40,50,60,70,80,90,100)
y = c(0,10,20,30,63,70,75,81,87,92,98,100)

x2 = x*x
x3 = x*x2
x4 = x*x3
x5 = x*x4
x6 = x*x5
lm = glm(y ~ x6 +x5+ x4 + x3 + x2 + x)
lm2 = glm (y ~ x3 + x2 +1)
y2 = predict.lm(lm)
plot(x,y)
lines(x,y2)
y3 = predict.lm(lm2)
lines(x,y3)    
```

When you save the notebook, an HTML file containing the code and output will be saved alongside it (click the *Preview* button or press *Ctrl+Shift+K* to preview the HTML file).
```{r}
scores =c(13,59,34,56,14,36,15,36,25,24,
9,41,41,84,4,29,24,66,7,64,13,16,11,55,
18,87,97,77,41,11,57,67,24,41,89,50,58,
21,14,43,60,49,30,75,30,98,13)

hist(scores,breaks=20)

```


```{r}

x = 1:50
xb = sort(c(x, x, x, x, x, x, x, x))
x2 = xb * xb
x3 = xb * xb * xb

y = xb*(xb-20)*(xb-40) + rnorm(400,xb*600,xb*200)


lg = glm(y ~ xb + x2 + x3)
plot(xb,y,col="#0000FF33",pch=19,
    xlab="time",ylab="responses")

y22 = predict.lm(lg)
lines(xb,y22,col="#FF000088",lwd=2)


```





