Fundamental Math
========================================================
title: Lecture notes: Robert Batzinger
author: IT100 Session 10: Trigonomic Functions
date: 1 Nov 2017
autosize: true
css: mathclass.css
font-import: http://fonts.google.com/css?family=Bree Serif
font-family: 'Bree Serif'


====================================
type: section

# Agenda

Agenda
===============

1. Solving Trigonometric Equations with Identities
2. Sum and Difference Identities
3. Double-Angle, Half-Angle, and Reduction Formulas
4. Sum-to-Product and Product-to-Sum Formulas
5. Solving Trigonometric Equations
6. Modeling with Trigonometric Equations

====================================
type: section

##  Your petition

Water Bottle Bungee Challenge
====================================
![plot of chunk unnamed-chunk-1](session10-figure/unnamed-chunk-1-1.png)

***

![plot of chunk unnamed-chunk-2](session10-figure/unnamed-chunk-2-1.png)

Mapping
=====================
left: 60%

![plot of chunk unnamed-chunk-3](session10-figure/unnamed-chunk-3-1.png)

***
$$\begin{array}{lrrr}
time & Angle\ A & Angle\ B & Angle\ C \\ 
9:10:00 &	58	&	4	&	118\\
9:10:10 &	15	&	1	&	164\\
9:10:20 &	-38	&	-2	&	-140\\
9:10:30 &	-60	&	-5	&	-115\\
9:10:40 &	-62	&	-8	&	-110\\
9:10:50 &	-68	&	-11	&	-101\\
9:11:00 &	-71	&	-14	&	-95\\
9:11:20 &	-73	&	-17	&	-90\\
9:11:30 &	-75	&	-20	&	-85\\
9:11:40 &	-77	&	-23	&	-80\\
9:11:50 &	-79	&	-26	&	-75\\
9:12:00 &	-81	&	-29	&	-70\\
\end{array}
$$



Line of Sight Distances
==================

$$\begin{array}{rrrr}
time & BC & AC  & AB \\
\hline
 0 & 3.71	& 0.31	& 3.97 \\
10 & 3.72	& 0.25	& 3.97 \\
20 & 3.73	& 0.22	& 3.97 \\
30 & 3.74	& 0.38	& 3.97 \\
40 & 3.74	& 0.59	& 3.97 \\
50 & 3.75	& 0.77	& 3.97 \\
60 & 3.77	& 0.96	& 3.97 \\
70 & 3.80	& 1.16	& 3.97 \\
80 & 3.85	& 1.36	& 3.97 \\
90 & 3.93	& 1.58	& 3.97 \\
100 & 4.03	& 1.80	& 3.97 \\
110 & 4.17	& 2.05	& 3.97 \\
\end{array}
$$

***
![plot of chunk unnamed-chunk-4](session10-figure/unnamed-chunk-4-1.png)



Height
===========

$$\begin{array}{lrrr}
Time &  Angle &  BC & Height\\
 0& 2 & 0.13 &133 \\
10& 6 & 0.39 &390 \\
20& 10 & 0.66 &660 \\
30&14 & 0.91 &914 \\
40&18 & 1.15 &1,153 \\
50&21 & 1.34 & 1,344 \\
60&24 & 1.53 &1,533 \\
70&26 & 1.66 &1,664 \\
80&28 & 1.81 &1,807 \\
90&30 & 1.96 &1,964 \\
100&30 & 2.02 & 2,017 \\
110&30 & 2.09 & 2,086 \\
\end{array}$$

***

![plot of chunk unnamed-chunk-5](session10-figure/unnamed-chunk-5-1.png)


====================================
type: section


##  Trigonometric Equations and Identities

Basic definitions
=================
left: 66%
$$\large\begin{array}{rrcll}
cosine: & \cos\ \theta &=&{x \over r} \\
sine: & \sin\ \theta &=&{y \over r} \\
tangent: & \tan\ \theta &=& {y \over x} \\
secant: & \sec\ \theta &=& {r \over x} \\
cosecant: &\csc\ \theta &=& {r \over y} \\
cotangent: &\cot\ \theta &=& {y \over x} \\
\end{array}$$

***
![triangle](img/adjopphyp.png)

Graphs
==============

![plot of chunk unnamed-chunk-6](session10-figure/unnamed-chunk-6-1.png)

***

![plot of chunk unnamed-chunk-7](session10-figure/unnamed-chunk-7-1.png)

Identities
===========

$$\large\begin{array}{rcl}
\sin\theta &=& \cos\left({\pi\over 2} - \theta\right) = \cos \left(90 -\theta\right)\\
\cos\theta &=& \sin\left({\pi\over 2} - \theta\right) = \sin \left(90 -\theta\right)\\
\tan\theta &=& \cot\left({\pi\over 2} - \theta\right) = \tan \left(90 -\theta\right)\\
\sin\left(\theta\right)^2 + \cos\left(\theta\right)^2 & = & 1\\
\end{array}$$

====================================
type: section

##  Half-Angle, Double-Angle, and Reduction Formulas


Half angle formulas
==================

$$\large\begin{array}{rcl}
\sin\left({\theta\over 2}\right) & = & \pm \sqrt{1- \cos(\theta)\over 2}\\
\\
\cos\left({\theta\over 2}\right) & = & \pm \sqrt{1 + \cos(\theta)\over 2}\\
\\
\tan\left({\theta\over 2}\right) & = & \pm \sqrt{1 - \cos(\theta)\over 1 + \cos(\theta)} = {\sin(\theta) \over 1 + cos(\theta)} = {1 - cos(\theta)\over \sin(\theta)}\\
\end{array}$$


Double angle formulas
======================

$$\large\begin{array}{rcl}
\sin\left(2\theta\right) & = & 2 \sin(\theta) \cos(\theta)\\
\\
\cos\left(2\theta\right) & = & \cos^2(\theta) - \sin^2(\theta) = 1 - 2 \sin^2(\theta) = 2 \cos^2(\theta) - 1\\
\\
\tan\left(2\theta\right) & = & {2 \tan(\theta)\over 1 - \tan^2(\theta)} \\
\end{array}$$

Reduction Formula
=======================

$$\large\begin{array}{rcl}
\sin^2 \theta & = & {1 - \cos(2\theta)\over 2}\\
\\
\cos^2 \theta & = & {1 + \cos(2\theta)\over 2}\\
\\
\tan^2 \theta & = & {1 - \cos(2\theta)\over 1 + \cos(2\theta)}\\
\end{array}$$


====================================
type: section

##  Sum, Difference, Product


Sum Formulas
===========================

$$\large\begin{array}{rcl}
\sin(\alpha + \beta) & = & \sin\alpha \cos\beta + \cos\alpha \sin\beta\\
\\
\cos(\alpha + \beta) & = & \cos\alpha \cos\beta - \sin\alpha \sin\beta\\
\\
\tan(\alpha + \beta) & = & {\tan\alpha + \tan\beta\over 1 - \tan\alpha \tan\beta}\\
\end{array}$$


Difference Functions
======================

**Difference**
$$\large\begin{array}{rcl}
\sin(\alpha - \beta) & = & \sin\alpha \cos\beta - \cos\alpha \sin\beta\\
\\
\cos(\alpha - \beta) & = & \cos\alpha \cos\beta + \sin\alpha \sin\beta\\
\\
\tan(\alpha - \beta) & = & {\tan\alpha - \tan\beta\over 1 + \tan\alpha \tan\beta}\\
\end{array}$$


Product to Sum
=========================

$$\large\begin{array}{rcl}
\cos\alpha\ \cos \beta & = & {\cos(\alpha -\beta) + \cos(\alpha + \beta)\over 2}\\
\\
\sin\alpha\ \sin \beta & = & {\cos(\alpha -\beta) - \cos(\alpha + \beta)\over 2}\\
\\
\sin\alpha\ \cos \beta & = & {\sin(\alpha +\beta) + \sin(\alpha - \beta)\over 2}\\
\\
\cos\alpha\ \sin \beta & = & {\sin(\alpha +\beta) - \sin(\alpha - \beta)\over 2}\\
\end{array}$$

Law of Sines
============
left:66%
$$\large {\sin \alpha\over a} = {\sin \beta\over b} = {\sin \gamma \over c} $$

$$\large {a \over \sin \alpha} = {b \over \sin \beta} = {c \over \sin \gamma} $$

***

![triangle angles](img/triangleangles.png)


Area of an oblique triangle
================
left: 50%

$$\large\begin{array}{rcl}
Area &=& {bc\ \sin \alpha\over 2}\\
&=&  {ac\ \sin \beta\over 2}\\
&=&  {ab\ \sin \gamma\over 2}\\
\end{array}$$

***

requires 2 sides and the angle between them

![triangle angles](img/triangleangles.png)

Heron's Formula for Area 
==================

Requires the lengths of all sides

$$\large\begin{array}{rcl}
Area & = & \sqrt{s \left(s-a\right) \left(s-b\right) \left(s-c\right)}\\
\\
where\\
\\
s & = & {a + b + c\over 2}\\
\end{array}
$$



Linear systems
==============

* Day 1: 120 baht
   * 3 coffee
   * 2 bottled tea
* Day 2: 75 baht
   * 2 coffee
   * 1 bottled tea

***
Equation:

$$\begin{array}{rcl}
3c + 2t &=& 120 \\
2c + 1t &=& 75 \\
\end{array}
$$

Matrix representation:

$$\begin{array}{ccc}
3 & 2 & 120 \\
2 & 1 & 75 \\
\end{array}
$$

Gaussian Elimination
====================

* Divide the first row  by the value in the first column 
* Use this adjusted row to clear the first column in subsequent rows
* Repeat for each subsequent column

$$\left[\begin{array}{ccc}
    3 & 2 & 120 \\ 
    2 & 1 &  75 \\
\end{array}\right]\Rightarrow
\left[\begin{array}{ccc}
   1 &  0.67 & 40  \\
  0 & -0.33 &  -5  \\
\end{array}\right]\Rightarrow
\left[\begin{array}{ccc}
  1 & 0.67  & 40 \\
  0 &  1    & 15 \\
\end{array}\right]
$$

Gaussian Elimination: Worked examples
========================

$$\begin{array}{ccc}
3 & 2 & 120 \\
2 & 1 & 75 \\
\\
1 & 2/3 & 40 \\
2 & 1 & 75 \\
\\
1 & 2/3 & 40 \\
2-1\times 2 & 1- 2 \times 2/3 & 75 - 2\times 40 \\
\\
1 & 2/3 & 40 \\
0 & -1/3 &  -5\\
\\
1 & 2/3 & 40 \\
0 & 1 & 15 \\
\end{array}
$$

Back substitution
=====================

* Use the values in the bottom row to remove the 
values in the column above.
* Repeat until there is an identity matrix
* The solution is the identity matrix

$$\left[\begin{array}{ccc}
1 & 2/3 & 40 \\
0 & 1 & 15 \\ 
\end{array}\right]\Rightarrow
\left[\begin{array}{ccc}
1 & 0 & 30 \\
0 & 1 & 15 \\
\end{array}\right]
$$


Back substitution: example
==================================

$$\begin{array}{ccc}
  1 & 0.67  & 40 \\
  0 &  1    & 15 \\
\\
1 & 2/3 - 2/3 & 40 - 15*2/3\\
0 & 1 & 15 \\
\\
1 & 0 & 30 \\
0 & 1 & 15 \\
\end{array}
$$


