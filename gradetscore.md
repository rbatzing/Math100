---
title: "R Notebook"
output: html_notebook
---

This is an [R Markdown](http://rmarkdown.rstudio.com) Notebook. When you execute code within the notebook, the results appear beneath the code. 

Try executing this chunk by clicking the *Run* button within the chunk or by placing your cursor inside it and pressing *Ctrl+Shift+Enter*. 

```{r}
# Remove W,5,7,11 scores for lack of attendance
finalscores = c(11,12,12,13,
14,15,19,20,22,24,28,28,
29,32,34,34,38,38,41,46,
47,52,54,65,68,74,81,84,
86,96,
9,9,12,29,
39,42,52,55,59,59,62,70,
76,97)

hist(finalscores,breaks=20)
```

Mean = 
```{r} 
mean(finalscores)
```

Std.Dev = 
```{r,exec=TRUE}
sd(finalscores)
```


```{r}


mean(finalscores)
sd(finalscores)


h = hclust(dist(finalscores))
h$labels = finalscores[h$order]
plot(h, col=c("red","green","blue"))

```

Add a new chunk by clicking the *Insert Chunk* button on the toolbar or by pressing *Ctrl+Alt+I*.

When you save the notebook, an HTML file containing the code and output will be saved alongside it (click the *Preview* button or press *Ctrl+Shift+K* to preview the HTML file).

The preview shows you a rendered HTML copy of the contents of the editor. Consequently, unlike *Knit*, *Preview* does not run any R code chunks. Instead, the output of the chunk when it was last run in the editor is displayed.
