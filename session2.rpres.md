Fundamental Math
========================================================
title: Lecture notes: Robert Batzinger
author: IT100 Session 2: Number systems
date: 5 Sept 2017
autosize: true
css: mathclass.css
font-import: http://fonts.google.com/css?family=Bree Serif
font-family: 'Bree Serif'

First Slide
========================================================






Topic for Unit 3
==============================

* **Arithmetic operations and properties**
  * Identity
  * Reciprocal
  * Associative
  * Communicative
  * Distributative

Topic for Unit 4
==============================

* **Simple arithmetic operations**
  * Scalars
  * Vectors
  

Topic for Unit 5
==============================

* **Algebraic expressions and equations**
  * Graphic approach
  * Numerical approaches)

Topic for Unit 6
==============================

* **Equations: properties  and functions**
  * Linear
  * Exponential
  * Logarithmic
  * Quadratic
  * Polynomial 
  * Cyclic
  * Trigonometric

Topic for Unit 7
==============================

* Graphs and Networks
  * Properties
  * Representation

* Systems of linear equations
  * Representation
  * Solution



For more details on authoring R presentations please visit <https://support.rstudio.com/hc/en-us/articles/200486468>.

- Bullet 1
- Bullet 2
- Bullet 3

Slide With Code
========================================================


```r
summary(cars)
```

```
     speed           dist       
 Min.   : 4.0   Min.   :  2.00  
 1st Qu.:12.0   1st Qu.: 26.00  
 Median :15.0   Median : 36.00  
 Mean   :15.4   Mean   : 42.98  
 3rd Qu.:19.0   3rd Qu.: 56.00  
 Max.   :25.0   Max.   :120.00  
```

Slide With Plot
========================================================

![plot of chunk unnamed-chunk-2](session2.rpres-figure/unnamed-chunk-2-1.png)
