Fundamental Math
========================================================
title: Lecture notes: Robert Batzinger
author: IT100 Session 9: Trigonomic Functions
date: 1 Nov 2017
autosize: true
css: mathclass.css
font-import: http://fonts.google.com/css?family=Bree Serif
font-family: 'Bree Serif'


====================================
type: section

# Agenda

Agenda
===============

0. Your petition
1. Solving Trigonometric Equations with Identities
2. Sum and Difference Identities
3. Double-Angle, Half-Angle, and Reduction Formulas
4. Sum-to-Product and Product-to-Sum Formulas
5. Solving Trigonometric Equations
6. Modeling with Trigonometric Equations

====================================
type: section

##  Your petition

Initial Session
==================

![word cloud](img/initialopinion.png)

Signatories
================

![signatories](img/signatures.png)

Petition
===============
![petition](img/petition.png)

Key issues
=====================

* Teaching methods
* The set up of the room
* Computers are a distraction
* Want to work with paper with pencil
* ???

Agreed Class Goals
================
* Pass this course
* Supplement knowledge 
* Prepare for other courses
* Assist in career suubjects
* Solve difficult problems with ease
* Able to explain math
* Develop a love for math
* Learn something new and practical

Class Rules
=============
* Help each other
* Understand respect each other 
* Work collectively
* Work on effective communications 
* Come ready to discuss the topic
* Commitment to achieve goals


Some questions
==================
* Who is this petition addressed to?
* What opinions/resources have you already explored?
* What do you really want?


Unit Test Results
====================
![plot of chunk unnamed-chunk-1](session9-figure/unnamed-chunk-1-1.png)


***

![plot of chunk unnamed-chunk-2](session9-figure/unnamed-chunk-2-1.png)

Composite Grade
======================

![plot of chunk unnamed-chunk-3](session9-figure/unnamed-chunk-3-1.png)

***

![plot of chunk unnamed-chunk-4](session9-figure/unnamed-chunk-4-1.png)


====================================
type: section

##  Trigonometric Equations and Identities

Basic definitions
=================
left: 75%
$$\large\begin{array}{rrcll}
cosine: & \cos\ \theta &=&{x \over r} & = {1\over\sec\ \theta}\\
sine: & \sin\ \theta &=&{y \over r} & = {1\over \csc\ \theta}\\\
tangent: & \tan\ \theta &=& {y \over x} & = {1\over \cot\ \theta}\\
secant: & \sec\ \theta &=& {r \over x} & = {1\over \cos\ \theta} \\
cosecant: &\csc\ \theta &=& {r \over y} & {1 \over \sin\ \theta}\\
cotangent: &\cot\ \theta &=& {y \over x}& {1 \over \tan\ \theta}\\
\end{array}$$

***
![triangle](img/adjopphyp.png)

Identities
===========

$$\large\begin{array}{rcl}
\sin\theta &=& \cos\left({\pi\over 2} - \theta\right) \\
\\
\cos\theta &=& \sin\left({\pi\over 2} - \theta\right) \\
\\
\tan\theta &=& \cot\left({\pi\over 2} - \theta\right) \\
\end{array}$$

====================================
type: section

##  Half-Angle, Double-Angle, and Reduction Formulas


Half angle formulas
==================

$$\large\begin{array}{rcl}
\sin\left({\theta\over 2}\right) & = & \pm \sqrt{1- \cos(\theta)\over 2}\\
\\
\cos\left({\theta\over 2}\right) & = & \pm \sqrt{1 + \cos(\theta)\over 2}\\
\\
\tan\left({\theta\over 2}\right) & = & \pm \sqrt{1 - \cos(\theta)\over 1 + \cos(\theta)} = {\sin(\theta) \over 1 + cos(\theta)} = {1 - cos(\theta)\over \sin(\theta)}\\
\end{array}$$


Double angle formulas
======================

$$\large\begin{array}{rcl}
\sin\left(2\theta\right) & = & 2 \sin(\theta) \cos(\theta)\\
\\
\cos\left(2\theta\right) & = & \cos^2(\theta) - \sin^2(\theta) = 1 - 2 \sin^2(\theta) = 2 \cos^2(\theta) - 1\\
\\
\tan\left(2\theta\right) & = & {2 \tan(\theta)\over 1 - \tan^2(\theta)} \\
\end{array}$$

Reduction Formula
=======================

$$\large\begin{array}{rcl}
\sin^2 \theta & = & {1 - \cos(2\theta)\over 2}\\
\\
\cos^2 \theta & = & {1 + \cos(2\theta)\over 2}\\
\\
\tan^2 \theta & = & {1 - \cos(2\theta)\over 1 + \cos(2\theta)}\\
\end{array}$$


====================================
type: section

##  Sum, Difference, Product


Sum Formulas
===========================

$$\large\begin{array}{rcl}
\sin(\alpha + \beta) & = & \sin\alpha \cos\beta + \cos\alpha \sin\beta\\
\\
\cos(\alpha + \beta) & = & \cos\alpha \cos\beta - \sin\alpha \sin\beta\\
\\
\tan(\alpha + \beta) & = & {\tan\alpha + \tan\beta\over 1 - \tan\alpha \tan\beta}\\
\end{array}$$


Difference Functions
======================

**Difference**
$$\large\begin{array}{rcl}
\sin(\alpha - \beta) & = & \sin\alpha \cos\beta - \cos\alpha \sin\beta\\
\\
\cos(\alpha - \beta) & = & \cos\alpha \cos\beta + \sin\alpha \sin\beta\\
\\
\tan(\alpha - \beta) & = & {\tan\alpha - \tan\beta\over 1 + \tan\alpha \tan\beta}\\
\end{array}$$


Product to Sum
=========================

$$\large\begin{array}{rcl}
\cos\alpha \cos \beta & = & {\cos(\alpha -\beta) + \cos(\alpha + \beta)\over 2}\\
\\
\sin\alpha \sin \beta & = & {\cos(\alpha -\beta) - \cos(\alpha + \beta)\over 2}\\
\\
\sin\alpha \cos \beta & = & {\sin(\alpha +\beta) + \sin(\alpha - \beta)\over 2}\\
\\
\cos\alpha \sin \beta & = & {\sin(\alpha +\beta) - \sin(\alpha - \beta)\over 2}\\
\end{array}$$



Law of Sines
============
left:66%
$$\large {\sin \alpha\over a} = {\sin \beta\over b} = {\sin \gamma \over c} $$

$$\large {a \over \sin \alpha} = {b \over \sin \beta} = {c \over \sin \gamma} $$

***

![triangle angles](img/triangleangles.png)


Area of an oblique triangle
================

![triangle angles](img/triangleangles.png)

$$\large Area = {bc\ \sin \alpha\over 2} =  {ac\ \sin \beta\over 2} =  {ab\ \sin \gamma\over 2}$$



Extra Credit Project 1
=====================
left: 50%

**Loi Krathong, 3 Nov**

* record the flight path of 2 balloons over 2 mins
* calculate the height and distance of the balloon
* plot the data
* submit a report
* reasonable submissions will get 5 points

***

![Loi Krathong](img/loi-krathong.jpg)


Requirements
===============
left: 75%
1. Create 2 soda straw sighting tubes with a plumb line
2. Form a team of 5 student.
   * Timekeeper with a flashlight to signal teams
   * 2 sighters with the slighting tubes
   * 2 protractor readers with flashlights
3. Separate slighting teams by 30 paces
4. Use the flashlight to synchronize readings every 10 sec measuring
5. Track the balloon for 2 mins

***

![Sightingtube](img/slightingtube.png)


Contents of the Report
================

* Names and roles of team members
* List the time and angle data collected for both teams 
   * the angle between the ground and the balloon
   * the angle between the balloon and the other measuring team
* List of the calculated distance and height
* A plot of the flight path
* Submit the report before class on 8 Nov


Calculations
========================
left: 33%

![blimp](img/blimp.png)

***

* Calculate missing angle:     
  $\large C = 180 - (70 + 60) = 50$
* Calculate length of the Segment $\large AC$:    
  $\large {AC \over \sin(60)} = {145\over \sin C} = {145\over \sin(50)}$
* Calculate height:    
  $\large ht = AC\ \sin(70)$
  


Extra Credit Project 2
======================
left: 30%

![Innovation](img/innovation.png)

***

**Bungee chord challenge**

* Student teams of 2 or 3 experiment with 10 rubber bands
* Create a linear mathematical model from the data
* Predict the number of rubber bands needed to create a bungee chord
* Test the performance of the bungee chord
* The closest entry without hitting the ground wins a prize
* Experimentation starts at 10AM, testing at 11AM


Report Contents
===============

* Names of the team member
* Test data from experiments with 10 rubber bands
* Plot of the stretch achieved for 1 to 10 bands
* The linear model suggested by the data
* Predicted number of rubber bands and the test results
* A group picture of the team assembling the bungee chord
* Printed submitted before class on 8 Nov
* Worth 5 points of the course
