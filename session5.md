Fundamental Math
========================================================
title: Lecture notes: Robert Batzinger
author: IT100 Session 5: Linear Equations
date: 5 Sept 2017
autosize: true
css: mathclass.css
font-import: http://fonts.google.com/css?family=Bree Serif
font-family: 'Bree Serif'


====================================
type: section

# Agenda

* Linear Functions
* Graphs of Linear Functions 
* Modeling with Linear Functions 
* Fitting Linear Models to Data

====================================
type: section

## 1. Linear Functions

Definition of a line
=======================

* A straight connection between any 2 points
* The intersection between 2 intersecting planes
* A relationship in which there is a constant multiplier between the  domain and the range.

Modelling constant change
=========================

![Intercept](img/speed.png)

***

![plot of chunk unnamed-chunk-1](session5-figure/unnamed-chunk-1-1.png)



Finding distance.
=================================
Between points in 2D space: $\large (x_1,y_1)$ and $\large (x_2,y_2)$

$$\large Dist = \sqrt{(x_2-x_1)^2 + (y_2- y_1)^2}$$

Between points in 3D space: $\large (x_1,y_1,z_1)$ and $\large (x_2,y_2,z_2)$

$$\large Dist = \sqrt{(x_2-x_1)^2 + (y_2- y_1)^2 + (z_2- z_1)^2}$$

Between points in 4D space: $\large (w_1,x_1,y_1,z_1)$ and $\large (w_1,x_2,y_2,z_2)$

$$\large Dist = \sqrt{(w_2-w_1)^2 + (x_2-x_1)^2 + (y_2- y_1)^2 + (z_2- z_1)^2}$$

A point to a line
========================

Distance between a point $\large (x_0,y_0)$ and a line $\large y= mx + b$

$$\large Dist = {|mx_0 + b - y_0|\over \sqrt{m^2 + 1}}$$


Finding the shortest path to a line
==========================
left: 60%

![plot of chunk unnamed-chunk-2](session5-figure/unnamed-chunk-2-1.png)
***

$$\large\begin{array}{rcl}
x_0&=& {(x +m y) - m b\over m^2 + 1}\\
& & \\
y_0 &=& {m(x + m y)+b\over m^2 + 1}\\
\end{array}$$


Step wise process
=======================
1. Determine the perpendicular line.
2. Solve for $x_0$ of the pivot point common to both the line and the perpendicular.
3. Solve for $\large y_0$ of the pivot point

***

$$\large\begin{array}{rcl}
y &=& 2x + 2\\
& & \\
y &=& - {1\over 2}x + b_p \\
5 &=& -{8\over 2} + b_p\\
9 &=& b_p\\
& &  \\
2 x_0 + 2 &=& -{1\over 2} x_0 + 9\\
{5\over 2} x_0 &=& 7\\
x_0 &=&{14\over 5} = 2.8\\
& & \\
y_0 &=& 2 \times 2.8 + 2 = 7.6\\
\end{array}$$

Shortest distance from point to line
===========================

![plot of chunk unnamed-chunk-3](session5-figure/unnamed-chunk-3-1.png)

***

$$\large\begin{array}{rcl}
Dist & = &\sqrt{(x_0 - x)^2 + (y_0 - y)^2 }\\
& & \\
Dist & = &\sqrt{(2.8 - 8)^2 + (7.6 - 5)^2 }\\
 &=& \sqrt{5.2^2 + 2.6^2} = \sqrt{33.8}\\
 &=& 5.8 \\
\end{array}$$




Applications
========================
left: 40%

![Intercept](img/bamboo.png)

***
* Growth of bamboo, hair, nails
* Rate of production
* Distance travelled
* Burn rate
* Conversions
* Exchange rates

Break even point
=====================
left: 20%
$$\large Revenue = Cost$$

***

![Intercept](img/breakeven.png)


Challenge 1
===========
left:30%
The Federal Helium Reserve held about $\large 16 \times 10^9$ cubic feet of helium in 2010 and is being depleted by about $\large 2.1 \times 10^9$ cubic feet each year.

***
* Give a linear equation for the remaining federal helium reserves, R, in terms of t, the number of years since 2010.

* What will the helium reserves be in 2015?

* When will the Federal Helium Reserve be depleted if the trend continues?


Challenge 2
===========
left:30%

Housing prices

|Year|Indiana|Alabama|
|----|-------|-------|
|1940|35,700|17,100|
|2000|94,300|87,100|

***

* In which state have home values increased at a higher rate?
* If these trends continued, what was the median home value in Indiana in 2010?
* In which year would we expect the two states’ median house values to be the same
?

Challenge 3
===========
left: 30%

In 2012, a guest house had 1,001 visitors. By 2016, the number of visitors grew to 
 1,697. Assume linear growth.
 
***

* How much did the number of visitors grow between 2012 and 2016?
* How long did it take for the number to grow from 1,001 to 1,697 visitors?
* What is the average growth rate per year?
* What was the number of visitors in the year 2000?
* Find the equation for the annual visitors, V, for   t years after 2000.
* Predict the number of visitors in 2020.




====================================
type: section

## 2. Graphs of Linear Functions


Specifying a line
=========================

![Intercept](img/line.png)


Changing the slope
===========================

![Intercept](img/slope.png)

Changing the intercept
===========================

![Intercept](img/lineshift.png)


Creating parallel lines
===========================

![Intercept](img/intercept.png)

Creating perpenticular lines
===============================
![Intercept](img/parallellines.png)


Challenge 1
===============
left: 30%

![graph](img/linegraph.png)

***

* Find the linear function y, where y depends on t, the number of years since 1980.
* Find and interpret the y-intercept.
* Find and interpret the x-intercept.
* Find and interpret the slope.




====================================
type: section

## 3. Modeling with Linear Functions

System of linear relations
==========================
![Extrapolation](img/linearsystem.png)


Extrapolation vs Interpolation
===============================

![Extrapolation](img/extrapolation.png)


Point to Line
==============

![Extrapolation](img/pointdist.png)


Challenge 1
===================
left: 80%

Use these data to answer the following questions

* What was the population in 2003? Is this calculation interpolation or extrapolation?
* Estimate when the population will reach 15,000. Is this calculation interpolation or extrapolation?

***

|Year | Pop |
|-----|-----|
|1990 | 11,500 |
|1995 | 12,100 |
|2000 | 12,700 |
|2005 | 13,000 |
|2010 | 13,750 |
   

Challenge 2
=================
left: 60%
![plot of chunk unnamed-chunk-4](session5-figure/unnamed-chunk-4-1.png)
***
* What is the slope of the line?
* What is the intercept?
* What is the distance between Point A and the line?
* What is the slope of the shortest path between A and the line?

Challenge 3
================
left: 30%

The nearest road is the $y= .33x + 20$

The 
village is located at $(10,30)$

***
* Find the point on the road closest to the village.
* Determine how far the village is from the road.


====================================
type: section

## 4. Fitting Linear Models to Data

Cricket Chirps
===============
left: 55%


```r
chirps=c(44,35,
  20.4,33,31,35,
  18.5,37,26)
temp =c(80.5,70.5,
  57,66,68,72,52,
  73.5,53)
plot(chirps,temp,
  pch=19,col="blue")
ln = 
  lm(temp ~ chirps)
abline(ln,col="red")
```
***
![plot of chunk unnamed-chunk-6](session5-figure/unnamed-chunk-6-1.png)
[Cricket chirping](https://www.youtube.com/watch?v=CQFEY9RIRJA)


Data analysis
=================
| | chirps(x) | temp (y) | (xy) | (x^2) | (y^2) |
|---|------|------|-------|-----|------|
|1 | 18.5 |52.0  |962.0  |342.25| 2704.00|
|2 | 20.4 |57.0 |1162.8 | 416.16| 3249.00|
|3 | 26.0 |53.0 |1378.0  |676.00| 2809.00|
|4 | 31.0 |68.0 |2108.0  |961.00| 4624.00|
|5 | 33.0 |66.0 |2178.0 |1089.00| 4356.00|
|6 | 35.0 |70.5 |2467.5 |1225.00| 4970.25|
|7 | 35.0 |72.0 |2520.0 |1225.00| 5184.00|
|8 | 37.0 |73.5 |2719.5 |1369.00| 5402.25|
|9 | 44.0 |80.5 |3542.0 |1936.00| 6480.25|
|$\sum$ |279.9 | 592.5|19037.80| 9239.4|39778.8|
|Mean|31.1|65.8|2115.3|1026.6|4419.9|

Regression Formula
================
left:45

$$\large\begin{array}{rcl}
\overline x &=& {\sum x \over n}\\
\\
\\
m &=& {\sum x y - \left(\overline x \sum y\right) \over \sum x^2 - n\left(\overline x\right)^2}\\
\\
\\
\\
b &=& \overline y - m \overline x\\
\end{array}$$

***
$$\large\begin{array}{rcl}
\overline x &=& {279.9 \over 9}\\
&=& 31.1\\
\\
m &=& {19037.8  - \left(31.1\times 592.5\right) \over 39778.8 - 9\left(31.1\right)^2}\\
&=& {19037.8 - 18426.8\over 9239.4 -8704.8} = {611.0 \over 534.2}\\
&=& 1.144\\
\\
b &=& 65.8 - 1.144\times 31.1\\
  &=& 65.8 - 35.6\\
  &=& 30.2\\
\end{array}$$

Meaning of Correlation
=======================

![Intercept](img/correlation.png)

Correlation
=======================
$$\large\begin{array}{rcl}
r &=& {\sum x y - n\bar x\bar y \over \sqrt{\left(\sum x^2 - n\bar x^2\right)}\ \sqrt{\left(\sum y^2 -n\bar y^2\right)}}\\
\\
r &=& {19037.80 - 9 \times 31.1 \times 65.8 \over \sqrt{\left(9239.4 - 9\times 31.1^2\right)}\sqrt{\left(39778.8 - 9\times 65.8^2\right)}}\\
  &=& {19037.80 - 18417.42 \over \sqrt{9239.4 - 8704.9)}\ \sqrt{39778.8 - 38966.76}}\\
  &=& {620.38 \over \sqrt{534.5}\ \sqrt{812.04}} = {620.38\over 23.12\times 28.5}\\
  &=& {620.38\over 658.92} = 0.9415\\
\end{array}$$

Analysis: temperature ~ chirps
============================

|Coefficients | Estimate| Std. Error |t value| Prob |
|-----|-----|-----|-----|-----|
|(Intercept) | 30.2806  |   4.5048  | 6.722 | 0.000272 |
| chirps   |     1.1432  |   0.1406  | 8.131| 8.22e-05|

|Statistic | Value|
|---------|-----|
| Residual standard error: |3.251 on 7 DF|
|Multiple R-squared: | 0.9043 |
|Adjusted R-squared: | 0.8906 |
|F-statistic: |66.11 on 1 and 7 DF|
| p-value: | 8.217e-05 |

|Residuals:|    Min |  1Q | Median | 3Q  |   Max |
|-----|------|---|----|----|----|
| |-7.0031 |-0.0803 | 0.5707| 1.7083|  3.3986 |

Challenge 1
==========
left: 75%

You are measuring the time required to weave silk cloth. 

* How long does it take to start up the weaving process?
* What is the average weaving rate once the process is started?
* How linear is the process?

***

| Time | cm |
|-----|-----|
|10|	3 |
|20|	7 |
|30|	55 |
|40|	60 |
|50|	95 |


Challenger 2
===============
left: 80%
Te U.S. Census tracks the percentage of persons 25 years or older who are college graduates. 


* Determine if the trend is linear.

* What line best describes the trend?

* If the trend
continues, in what year will the percentage exceed 35%?

***

|Year | % Grad |
|----|------|
|1990|21.3 | 
|1992|21.4 | 
|1994| 22.2 |
|1996| 23.6 |
|1998| 24.4 |
|2000| 25.6 |
|2002| 26.7 |
|2004| 27.7 |
|2006| 28.0 |
|2008|29,4 |

Challenge 3
=========================
Explain the disappearing square.

![square](img/Missing_Square.gif)


====================================
type: section

## 5. Next Time

Reading for next time
======================
* 3.1 Complex Numbers
* 3.2 Quadratic Functions
* 3.3 Power Functions and Polynomial Functions
* 3.4 Graphs of Polynomial Functions
* 3.5 Dividing Polynomials

***
* 3.6 Zeros of Polynomial Functions
* 3.7 Rational Functions
* 3.8 Inverses and Radical Functions
* 3.9 Modeling Using Variation
