Fundamental Math
========================================================
title: Lecture notes: Robert Batzinger
author: IT100 Session 7: Exponent and Logarithmic systems
date: 5 Sept 2017
autosize: true
css: mathclass.css
font-import: http://fonts.google.com/css?family=Bree Serif
font-family: 'Bree Serif'


====================================
type: section

# Agenda

Agenda
==========================

* Exponential Functions
* Logarithmic Functions 
* Graphs of logarithmic Functions 
* Exponential and logarithmic Models
* Fitting exponential Models to Data

====================================
type: section

## Exponential Functions (4.1)

Binary Growth rates
==========================
$$\begin{array}{rccc}
0&2^0&1&o\\
1&2^1&2&oo\\
2&2^2&4&oooo\\
3&2^3&8&oooooooo\\
4&2^4&16&oooooooooooooooo\\
5&2^5&32&oooooooooooooooooooooooooooooooo\\
6&2^5&64&oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo\\
\end{array}$$


Exponential Growth
========================
left: 40%

![plot of chunk unnamed-chunk-1](session7-figure/unnamed-chunk-1-1.png)
***

| Rate | 1 | 2 | 3 | 4 | 5 | 6 |
|------|----|----|---|---|---|---|
| $\large 1^n$ | 1 | 1 | 1 | 1 | 1 | 1 |
| $\large 2^n$ | 2 | 4 | 8 | 16 | 32 | 64 |
| $\large 4^n$ | 3 | 9 | 27 | 81 | 243 | 729 |
| $\large 6^n$ | 6 | 36 | 216 | 1296 | 7776 | 46656 |
| $\large 8^n$ | 8 | 64 | 512 | 4096 | 32768 | 262144 |
| $\large 10^n$ | 10 | 100 | 1000 | 10000 | 100000 | 1000000 |
| $\large 12^n$ | 12 | 144 | 1728 | 20736 | 248832 | 2985984 |


Exponential Growth
=========================

![plot of chunk unnamed-chunk-2](session7-figure/unnamed-chunk-2-1.png)
***

![plot of chunk unnamed-chunk-3](session7-figure/unnamed-chunk-3-1.png)

Interest Rate Growth
==================
left: 40%

![plot of chunk unnamed-chunk-4](session7-figure/unnamed-chunk-4-1.png)
***

|Rate|1yr|	2yr|	3yr|	4yr|	5yr|	6yr|	7yr|	8yr|
|----|----|----|-----|-----|-----|-----|-----|-----|
|1% |1.01	|1.02	|1.03	|1.04	|1.05	|1.06	|1.07	|1.08 |
|5% |1.05	|1.10	|1.16	|1.22	|1.28	|1.34	|1.41	|1.48 | 
|10% |1.10	|1.21	|1.33	|1.46 |1.61	|1.77	|1.95	|2.14 |
|15% |1.15	|1.32	|1.52	|1.75	|2.01	|2.31	|2.66	|3.06 |
|20% |1.20	|1.44	|1.73	|2.07	|2.49	|2.98	|3.58	|4.30 |
|25% |1.25	|1.56	|1.95	|2.44	|3.05	|3.81	|4.77	|5.96 |
|30% |1.30	|1.69	|2.20	|2.86	|3.71	|4.83	|6.27	|8.16 |

Interest Rate Growth
==========================

![plot of chunk unnamed-chunk-5](session7-figure/unnamed-chunk-5-1.png)
***
![plot of chunk unnamed-chunk-6](session7-figure/unnamed-chunk-6-1.png)


Degradation Rates
==================

![plot of chunk unnamed-chunk-7](session7-figure/unnamed-chunk-7-1.png)
***

| Rate | 1 | 2 | 3 | 4 | 5 | 6 |
|------|---|---|---|---|---|---|
| 1/2 | 50.0000 | 25.0000 | 12.5000 | 6.2500 | 3.1250 | 1.5625 |
| 1/4 | 25.0000 | 6.2500 | 1.5625 | 0.3906 | 0.0977 | 0.0244 |
| 1/6 | 16.6667 | 2.7778 | 0.4630 | 0.0772 | 0.0129 | 0.0021 |
| 1/8 | 12.5000 | 1.5625 | 0.1953 | 0.0244 | 0.0031 | 0.0004 |
| 1/10 | 10.0000 | 1.0000 | 0.1000 | 0.0100 | 0.0010 | 0.0001 |
| 1/12 | 8.3333 | 0.6944 | 0.0579 | 0.0048 | 0.0004 | 0.0000 |


Degradation Rates
=======================

![plot of chunk unnamed-chunk-8](session7-figure/unnamed-chunk-8-1.png)

***

![plot of chunk unnamed-chunk-9](session7-figure/unnamed-chunk-9-1.png)

Degradation as percentage
=========================
left: 40%

![plot of chunk unnamed-chunk-10](session7-figure/unnamed-chunk-10-1.png)

***

|Rate|	1yr	|2yr	|3yr	|4yr	|5yr	|6yr	|7yr	|8yr|
|----|----|----|----|----|----|----|----|----|
|-1% | 99.0 | 98.0 | 97.0 | 96.1 | 95.1 | 94.1 | 93.2 | 92.3|
|-5% | 95.0 | 90.3 | 85.7 | 81.5 | 77.4 | 73.5 | 69.8 | 66.3|
|-10% | 90.0 | 81.0 | 72.9 | 65.6 | 59.0 | 53.1 | 47.8 | 43.0|
|-15% | 85.0 | 72.3 | 61.4 | 52.2 | 44.4 | 37.7 | 32.1 | 27.2|
|-20% | 80.0 | 64.0 | 51.2 | 41.0 | 32.8 | 26.2 | 21.0 | 16.8|
|-25% | 75.0 | 56.3 | 42.2 | 31.6 | 23.7 | 17.8 | 13.3 | 10.0|
|-30%	|70.0	|49.0	|34.3	|24.0	|16.8	|11.8	|8.2	|5.8|


Degradation as percentage
=========================

![plot of chunk unnamed-chunk-11](session7-figure/unnamed-chunk-11-1.png)

***

![plot of chunk unnamed-chunk-12](session7-figure/unnamed-chunk-12-1.png)
Exponent Rules
=============
$$\Large\begin{array}{rc}
\hbox{Product Rule} & {a^m\times a^n} = a^{m+n} \\
\hbox{Quotient Rule} & {a^m\over a^n} = a^{m-n} \\
\hbox{Product Rule} & (a^m)^n = a^{m \times n} \\
\hbox{Zero Exponent Rule} & a^0 = 1 \\
\hbox{Negative Exponent Rule} & a^{-n} = {1\over a^n} \\
\hbox{Power of ratio} & \left({a\over b}\right)^n = {a^n \over b^n}\\
\end{array}$$

Population Growth
===================
left: 60%

$$\Large N(t) = P \ b^t$$

* $\Large N$ : Population at time $\Large t$
* $\Large P$ : Initial population
* $\Large b$ : base of population growth
* $\Large t$ : Time in years

***

**Population of 3 Provinces **

| Provinces | 1999 |2014 | 2016 |
|------|---------|-------|------|
| Chiang Mai | 1,587,465 |1,678,284 |1,735,762 |
| Songkla |1,223,833 | 1,401,303  |  1,417,440 |
| Khon Khan |1,747,730 |1,790,049 | 1,801,753 |
| Phuket | 241,489 | 378,364| 386,605|

Calculations
==============================


$$\large\begin{array}{rclcrclrl}
1.736 &=& 1.587\ b_{cm}^{17}& &1.678 &=& 1.587\ b_{cm}^{15} \\
1.093888 &=& b_{cm}^{17}& & 1.057341 &= & b_{cm}^{15}\\
1.00529&=&b_{cm} & & 1.00372 &=&  b_{cm} &b_{cm} & =1.004505 \\
\\
1.417 &=& 1.224 b_{sng}^{17} & & 1.401 &=& 1.224 b_{sng}^{15}\\
1.15768 &=& b_{sng}^{17} & & 1.14461 &=& b_{sng}^{15}\\
1.00865 &=& b_{sng} & & 1.009045 &=& b_{sng}&b_{sng}&=1.00847\\
\\
1.801 &=& 1.747\ b_{kk}^{17} & & 1.790  &=& 1.747\ b_{kk}^{15}\\
1.03091 &=& b_{kk}^{17} & & 1.024614  &=&  b_{kk}^{15}\\
1.001792 &=& b_{kk} & & 1.001622  &=&  b_{kk}&b_{kk} &= 1.001707 \\
\\
0.387 &=& 0.241\ b_{pk}^{17} & & 0.378  &=& 0.241\ b_{pk}^{15}\\
1.605809 &=& b_{pk}^{17} & & 1.568465  &=&  b_{pk}^{15}\\
1.028252 &=& b_{pk} & & 1.030461  &=&  b_{pk}&b_{pk} &= 1.029357 \\
\end{array}$$

Population Modelling
==========================
![plot of chunk unnamed-chunk-13](session7-figure/unnamed-chunk-13-1.png)

Challenge
==================
left: 40%

![plot of chunk unnamed-chunk-14](session7-figure/unnamed-chunk-14-1.png)
***
**Device on Internet**

In 2005 there were 5 million devices connected to the Internet in Thailand. In 2017 the number of devices has grown to 35 million. 

* Determine the average annual growth rate assuming linear growth. 

* Determine the annual rate of growth assuming constant exponential grown.

* Estimate the number of devices connected in the year 2020?



Compound Interest
==========================
$$\Large A(t) = P\left(1+{r\over n}\right)^{nt}$$


* $\Large P$ : Principle
* $\Large t$ : Time in years
* $\Large r$ : Annual Percentage Rate (APR)
* $\Large n$ : Compound periods per year

***

![plot of chunk unnamed-chunk-15](session7-figure/unnamed-chunk-15-1.png)

====================================
type: section

## Logarithmic Functions (4.3)


Logarithmic conversions
=========================

$$\Huge\begin{array}{rcl}
y &=& \log_{10}(x)\\ 
x &=& 10^y\\
x_1 \times x_2 &=& 10^{\left(log(x_1)+log(x_2)\right)}\\
\sqrt[4]{x} &=& 10^{{\log_{10}(x)\over 4}}\\
\end{array}$$


Log Scale
==========

![plot of chunk unnamed-chunk-16](session7-figure/unnamed-chunk-16-1.png)

Slide rule
=================


![sliderule](img/sliderule.png)

Slide rule
=================
left: 30%
![sliderule](img/sliderule.png)
***
* Based on corresponding log10 of the C/D scales
* Adding L values equals the product of numbers
* Multiplication of L value by 2 used for $\large x^2$
* Division of L value by 2 used for $\large \sqrt{x}$


Challenge
=========
Find the following values using the virtual slide rule:

$$\Large \begin{array}{lrclr}
Area of circle: & A & = & \pi r^2 = \pi \times 3^2  & ......(1)\\
Ratios: & {a\over b} &=& {2\over 5} = {?\over 40} = {?\over 10} = {20\over ?}& ......(2)\\
Volume of a box: & h \times l \times 2 &=& 5  \times 15 \times 20&.....(3)\\
Volume of a cylinder:& \pi r^2 h &=& \pi 4^2 \times 10 & ......(4)\\
\end{array}$$



Log-Linear and Log-log plots
============

![plot of chunk unnamed-chunk-17](session7-figure/unnamed-chunk-17-1.png)

***

![plot of chunk unnamed-chunk-18](session7-figure/unnamed-chunk-18-1.png)



====================================
type: section

##  Graphs (4.2, 4.4) 




Linear - Linear Plot
======================
$$\Large \begin{array}{rcl}
y &=& 3x\\
y &=& 25/x\\
y &=& x^2\\
y &=& x^3\\
y&=& 2^x\\
y&=& 3^x\\
\end{array}$$

***

![plot of chunk unnamed-chunk-19](session7-figure/unnamed-chunk-19-1.png)

Log Plots
==================

![plot of chunk unnamed-chunk-20](session7-figure/unnamed-chunk-20-1.png)
***

![plot of chunk unnamed-chunk-21](session7-figure/unnamed-chunk-21-1.png)

Challenge
===================

* Note the scale of the linear-linear,log-linear and log-log plots

* Determine which relationships become linear in log-linear plots

* Determine which relationships become linear in log-log plots 

* Note the angles and shape of the curves in each of the plots 

====================================
type: section

##  Exponential and Logarithmic Models (4.7)


Challenge
==========

* Plot the data for Population A and B
* Determine the best trendline
* Determine the R^2 for the trendline
* Identify which population is moving toward steady-state

