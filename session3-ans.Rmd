---
title: 'IT100 Session 3: - Mathematical operations'
author: 'Answer key: Robert Batzinger'
output:
  html_notebook:
    autosize: yes
    self_contained: yes
    toc: yes
    toc_depth: 3
    toc_float: yes

  pdf_document:
    toc: yes
    toc_depth: 3
---

# Unit 1. Order of Precedence


## So what is the answer?
$$\large\begin{array}{rclr}
3 \times 4 - 2 \times 5 & = & (3\times 4)-(2\times 5) &(1)\\
(3 \times 4) - (2 \times 5) & = & 12 - 10 = 2 &(2)\\
\left((3 \times 4) - 2\right) \times 5 & = & (12 -2)\times 5 = 50 & (3)\\
3 \times (4 - 2) \times 5 & = & 3\times 2\times 5 = 30  & (4)\\
3 \times \left(4 - (2 \times 5)\right) & = & 3\times (4 - 10)= -18 & (5)\\
\end{array}$$


## Challenge questions
$$\large\begin{array}{rclr}
10 - 2^2 + 3 &=& 10 - 4 + 3= 9 &(1)\\
& & \\
{3^2 \times 7 - 3 + 6 \over 4 + 2} &=& {(9\times 7)- 3 + 6\over 6} = 66 \div 6 = 11 & (2)\\
\end{array}$$

# Unit 2: Exponents

## Challenge questions

Simplify the following:

$$\begin{array}{rclr}
{1 \over \sqrt{x}} & = & {1\over x^{1\over 2}} = x^{-{1\over 2}} & (1)\\
{2^x \times 8^2 \over 16} & = & {2^x \times (2^3)^2 \over 2^4} = 2^x \times 2^6 \times 2^{-4} = 2^{(x+2)} & (2)\\
\end{array}$$

3) The human heart beats about 70 times per min. Express in scientific notation, the number of times a heart would be expected to beat in 72 years.

$$\begin{array}{l}
{70\over min} \times {60 min\over hr} \times {24 hr\over day} \times {365 days\over yr} \times {72 yr} &\\
\qquad = 7\times 10^1 \times 6 \times 10^1 \times 2.4 \times 10^1 \times 3.65 \times 10^2 \times 7.2  \times 10^1 \\
\qquad = 7\times 6 \times 2.4 \times 3.65 \times 7.2  \times 10^{(1+1+1+2+1)} \\
\qquad = 26490 \times 10^{6} = 2.649 \times 10^4 \times 10^6 \\
\qquad = 2.649 \times 10^{10}\\
\end{array}$$

# Unit 3: Fractions


$$\large\begin{array}{rclr}
2{2\over 3} \times {1\over 5} + 1{1\over 2} \div 2{1\over 2} & = & {8\over 3 \times 5} + {{3\over 2}\over {5\over 2}} = {8\over 15} + {9\over15} = {17 \over 15} & (1)\\
& & \\
{{\left({1\over 3} \right)^2}\times 4{1\over 2}\over 3} & = & {{1\over 9} \times {9\over 2}\over 3} = {2 \times {1\over 2}\over 2 \times 3} = {1\over 6} & (2)\\
\end{array}$$


# Unit 4: Identity

\## Problem of Identity

Give an example and determine the value of $\large I$ for each relationship.

$$\large\begin{array}{rlrcrlr}
a \times I = a & I = 1 & (1) & & {a \over I} = a & I = 1 & (2)\\
a - I = a & I = 0 &(3) &  & a + I = a & I = 0 &(4)\\
a^I = a & I = 1 &(5) & & a \times a^I = a & I=0 & (6)\\
\end{array}$$

# Unit 5: Reciprocal

## Determining the reciprocal
Identify the reciprocal for each relationship.

$$\large\begin{array}{rlcrl}
a \times I = 1 & I = {1\over a} & (1) & & {a \over I} = 1 & I = a & (2)\\
a - I = 0 & I = a & (3) &  & a + I = 0 & I = -a & (4)\\
a \times a^I = 1 & I = -1 & (5) & & a \times a^I = a & I = 1 & (6)\\
\end{array}$$


# Unit 6: Basic laws of Math


## Producing a trail mix


## Recipe

|Volume | Ingredient  | Cost / 250 gm |     
|----|------|------| 
|100 gm |roasted almonds | $4.00 |    	1.60 |
|100 gm |roasted pecans  | $6.50 |  	2.60 |
|100 gm |roasted cashews | $4.75 |    	1.90 |
|100 gm |sunflower seeds | $0.40 |   	0.16 |
|100 gm |pumpkins seeds  | $0.75 |  	0.30 |
|150 gm |dried cherries  | $3.90 | 	2.34 |
|200 gm |raisin | $5.60 |	4.48 |
|100 gm |cranberries | $6.90 |	2.76 |
|200 gm |chocolate chips | $3.50|   	2.80 |
|10 gm |sea salt |$0.35 |   	0.01 |
| 5 gm |cinnamon | $8.00 |   	0.16 |
| 5 gm |grown nutmeg | $9.50 |	0.19 |
|1170 gm|Totals | - |	19.30 |
	

## Challenge Questions

* What is the cost per kilo?

$${19.30\over 1170} = 16.50 \qquad \qquad(1)$$

* If the market value is US$25.00 per 500 gm, what is the profit margin?

$$\hbox{Income:}\quad {25\over 500gm} = {50\over 1kg} \qquad\qquad (2)$$
$$\hbox{Cost:}\quad 16.50\qquad\qquad\qquad$$
$$\hbox{Profit:}\qquad 50.00 - 16.50 = 33.50 $$
$$\hbox{Profit margin:}\quad {33.50 \over 50} = 67\%$$


## Pound Cake

$$\large\begin{array}{lrr}
Ingredient & Amt/ kg & Cost/kg\\
Butter & 1	& 2.5	\\			
Eggs & 1 &	3.6	\\			
Flour & 1	& 1.5	\\			
Milk & 1 &	3.8	\\			
Sugar & 1 &	1.8	\\
\hfill Totals & 5 kg & 13.2 \\
\hfill Cost/kg & & {13.20 \over 5} = 2.64 \\
\end{array}$$
