Fundamental Math
========================================================
title: Lecture notes: Robert Batzinger
author: IT100 Session 6: Polynomial and Rational Functions
date: 5 Sept 2017
autosize: true
css: mathclass.css
font-import: http://fonts.google.com/css?family=Bree Serif
font-family: 'Bree Serif'


====================================
type: section

# Agenda



Agenda: Chapter 3
======================

1. Complex Numbers
2. Quadratic Functions
3. Power Functions and Polynomial Functions
4. Graphs of Polynomial Functions
5. Dividing Polynomials

***

6. Zeros of Polynomial Functions
7. Rational Functions
8. Inverses and Radical Functions
9. Modeling Using Variation 

====================================
type: section

# 3.1 Complex Numbers

Graphing imaginary numbers
==========================

![imaginary](img/imagine.png)

***

$$\large 3-4\sqrt{-1} =  3-4i$$

![imaginary](img/graph3and4i.png)


Basic arithmetic with imaginary numbers
===========================

$$\large\begin{array}{rcl}
(4 + 3i) + (2 - i) & = & (4+2)
 + (3-1)i= 6 +2i\\
 \\
 (4 + 3i) \times (2 - i) & = & 4 \times 2 + (6-4)i -3i^2 \\
 &=& 8 + 2i - (-3) = 11 + 2i\\
 \\
 (4 + 3i) - (2 - i) & = & (4-2)
 + (3+1)i= 2 +4i\\
 \\
{5\over 2-i} &=& {4 - i^2 \over 2-i} = {(2 - i)(2+i)\over 2-i} = 2 + i \\
 \end{array}$$


Challenge
===============
$$\large\begin{array}{rclr}
(5 + 3i)(5 - 3i) &=&........ &(1)\\
\\
{10\over 3-i} &=&........ &(2)\\
\\
(5 + 3i)-(2-2i)&=&........ &(3)\\
\end{array}$$



====================================
type: section

# 3.2 Quadratic Functions


Basic Parabola
=====================
left: 40%
$$\large\begin{array}{lcr}
y &=& (x+1)(x-3)\\
 & =& x^2 -2x -3\\
\end{array}$$

***
![equation](img/equation.png)

Parabolic Solar Cooker
=========================
![equation](img/parabolalens.png)

***

Old CDs on satellite dish
$$\large 400^{\circ}C \hbox{ or } 720^{\circ}F$$

![eq](img/solarcook.png)

Different quadratic curves
=====================
![plot of chunk unnamed-chunk-1](session6-figure/unnamed-chunk-1-1.png)
***
![](img/gravity.png)


Roots
=======
![](img/roots.png)

Quadratic Equation
====================

**Formula:**

$$\large\begin{array}{rcl}
0 &=& ax^2 + bx + c\\
\\
x &=& {-b \pm \sqrt{b^2 - 4ac}\over 2a}\\
\end{array}$$

***
**Example:**

$$\large\begin{array}{rcl}
0 &=& 6x^2  + 5x + 1\\
\\
x &=& {-5 \pm \sqrt{5^2 - 24}\over 12}\\
&=& {-5 \pm 1\over12}\\
&=& -{1\over 2}, -{1\over 3}\\
\end{array}$$


Challenge
============
Find the roots of these equations:

$$\large\begin{array}{rclr}
0&=&x^2-7x+10&....... (1)\\
\\
21&=&x^2-4x&....... (2)\\
\\
-3 &=& 4x^2 + -22x +7&....... (3)\\
\end{array}$$



====================================
type: section

# 3.3 Power Functions and Polynomial Functions


Effects of power
====================

![plot of chunk unnamed-chunk-2](session6-figure/unnamed-chunk-2-1.png)


====================================
type: section

# 3.4 Graphs of Polynomial Functions

Type of functions
==================
![](img/evenoddfunct.png)


Challenge
===================
Identify Odd vs Even and direction

$$\large\begin{array}{rclr}
y&=&x^3-x^2-7x+10&....... (1)\\
\\
21&=&x^4+x^3-x^2+x+yx&....... (2)\\
\\
y-3 &=& 4x^2 + -22x +7&....... (1)\\
\end{array}$$


====================================
type: section

# 3.5 Dividing Polynomials


Division of polynomials
===================

$$\large {2x^3 − 3x^2 + 4x + 5 \over x+2}$$

$$\large\begin{array}{cccccccccc}
\hbox{Ans:} & & 2x^2 &+& -7x &+& 18\\
\\
(x + 2)& &2x^3 &−& 3x^2 &+& 4x &+& 5\\
\\
&−(&2x^3 &+& 4x^2&)\\
&& & &−7x^2 &+& 4x\\
&& & −(&−7x^2& -& 14x&)\\
&& & & & & 18x &+& 5\\
&& & & & −(&18x &+& 36&)\\
&& & & & & & & −31\\
\end{array}$$

Challenge
==========


$$\large\begin{array}{rclr}
{x^4+6x^3-12x^2+10+5\over x-1} &=& .......& (1)\\
\\
{x^4 + 4x^3 + 3x^2 -4x -4\over x^2 + 4x + 4} &=& ........ & (2)\\
\end{array}$$




====================================
type: section

# 3.9 Modeling 


Data modelling
====================

![xx](img/datamodel.png)


Data modelling
====================
![xx](img/dataset.png)


Dow Jones Industrial Average
======================

![plot of chunk unnamed-chunk-3](session6-figure/unnamed-chunk-3-1.png)
***

![plot of chunk unnamed-chunk-4](session6-figure/unnamed-chunk-4-1.png)



====================================
type: section

# Preparation for Unit 7

Reading
==============================

Chapter 4: Exponential and Logarithmic Functions 
