Fundamental Math
========================================================
title: Lecture notes: Robert Batzinger
author: IT100 Session 3: Mathematical operations
date: 13 Sept 2017
autosize: true
css: mathclass.css
font-import: http://fonts.google.com/css?family=Bree Serif
font-family: 'Bree Serif'


====================================
type: section

# Basic arithmetic concepts


Agenda for Today
==================================
**Basic operations**
* Precedent of operators
* Exponents
* Fractions
* Addition and subtraction
* Multiplication and division

***

**Basic properties**
* Identity
* Reciprocal
* Associative property
* Communicative property
* Distributive property
  

====================================
type: section

# Unit 1. Order of Precedence


So what is the answer?
====================================
$$\large\begin{eqnarray}
3 \times 4 - 2 \times 5 & = & \hbox{............ (1)}\\
& & \\
(3 \times 4) - (2 \times 5) & = & \hbox{............ (2)}\\
\left((3 \times 4) - 2\right) \times 5 & = & \hbox{............ (3)}\\
3 \times (4 - 2) \times 5 & = & \hbox{............ (4)}\\
3 \times \left(4 - (2 \times 5)\right) & = & \hbox{............ (5)}\\
\end{eqnarray}$$

So what is the standard order?
=============

Mnemonic Key for Memorization
* Please
* Entertain
* My / Dear
* Aunt / Sally!!!

***
Standard Math Precedence
* Parenthesis
* Exponents
* Multiplication / Division
* Addition / Substraction
 
Challenge questions
==================
$$\large\begin{eqnarray}
10 - 2^2 + 3 &=& \hbox{........... (1)}\\
& & \\
{3^2 \times 7 - 3 + 6 \over 4 + 2} &=& \hbox{........... (2)}\\
\end{eqnarray}$$

====================================
type: section

# Unit 2: Exponents

Overview of exponents
===========================
$$\large\begin{eqnarray}
10^2 \times 10^3 & = & 10^5\\
100 \times 1000 & = & 100,000\\
 & & \\
{10^{5}\over 10^2}  =  10^5 \times 10^{-2} & = & 10^{(5-2)} = 10^3\\
 & & \\
\left(10^3\right)^2 = 10^3 \times 10^3 & = & 10^{3+3} = 10^6 \\
\end{eqnarray}$$

Multiplication with exponents 
==================
$$\large\begin{eqnarray}
10^2 \times 10^3 \times 10^4 & = & 10^{(2 + 3 + 4)} = 10^7\\
\\
a^3 \times a^2 \times a & = & a^{(3 + 2 + 1)} = a^6\\
\\
a^x \times a^y \times a^z & = & a^{(x+y+z)}\\
\end{eqnarray}$$

Division with exponents
==================

$$\large\begin{eqnarray}
{a^{7} \over a^{2}} = a^7 \times a^{-2} & = & a^{(7-2)} = a^5\\
\\
{a^{x} \over a^{y}} = a^x \times a^{-y} & = & a^{(x-y)}\\
\end{eqnarray}$$


Challenge questions
=======================

Simplify the following:

$$\large\begin{eqnarray}
{1 \over \sqrt{x}} & = & \hbox{......... (1)}\\
 & & \\
{2^x \times 8^2 \over 16} & = & \hbox{......... (2)}\\
\end{eqnarray}$$

***
3) The human heart beats about 70 times per min. Express in scientific notation, the number of times a heart would be expected to beat in 72 years.

====================================
type: section

# Unit 3: Fractions


Addition and subtraction with fractions
====================================

$$\large\begin{eqnarray}
{1\over 2} + {1\over 4} - {3\over 8} =  {4 \over 8} + {2\over 8} - {3\over 8} & = & {4 + 2 -3 \over 8} = {3\over 8}\\
& & \\
1{1\over 3} + 4{1\over6} = {3 + 1\over 3}+ {24+1\over 6} = {4\over 3} + {25\over 6} & = & {8 + 25\over 6}  = {33\over 6} = {11 \over 2}\\ 
\end{eqnarray}$$




Multiplication and division of fractions
==================================

$$\large\begin{eqnarray}
{1\over 2} \times {2\over 5} \times {3\over 8} & = &  {1 \times 2 \times 3 \over 2 \times 5 \times 8} = {6 \over 80} = {3\over 40}\\
& & \\
{2{1 \over 3} \over 4{2\over 5}} = {{7\over 3}\over {22\over 5}} & = & {7 \times 5\over 22 \times 3} = {35\over 66}\\
\end{eqnarray}$$


Challenge questions
==================

$$\large\begin{eqnarray}
2{2\over 3} \times {1\over 5} + 1{1\over 2} \div 2{1\over 2} & = & \hbox{.......... (1)}\\
& & \\
{{\left({1\over 3} \right)^2}\times 4{1\over 2}\over 3} & = & \hbox{.......... (2)}\\
\end{eqnarray}$$


====================================
type: section

# Unit 4: Identity

Definition of Identity
===================================

Identity for an operation is the quantity that will not change the value of number 

$$\large 3 \times 1 = 3$$

Problem of Identity
====================================

Give an example and determine the value of $\large I$ for each relationship.

$$\large\begin{array}{rlcrl}
a \times I = a & \hbox{.......... (1)} & & {a \over I} = a & \hbox{.......... (2)}\\
a - I = a & \hbox{.......... (3)} &  & a + I = a & \hbox{.......... (4)}\\
a^I = a & \hbox{.......... (5)} & & a \times a^I = a & \hbox{.......... (6)}\\
\end{array}$$

====================================
type: section

# Unit 5: Reciprocal



Definition of Reciprocal
====================================

A quantity that will cause an operation result in the identity value.

$$\large 4 \times {1\over 4} = 1 $$

Determining the reciprocal
========================================
Identify the reciprocal for each relationship.

$$\large\begin{array}{rlcrl}
a \times I = a & \hbox{.......... (1)} & & {a \over I} = a & \hbox{.......... (2)}\\
a - I = a & \hbox{.......... (3)} &  & a + I = a & \hbox{.......... (4)}\\
a^I = a & \hbox{.......... (5)} & & a \times a^I = a & \hbox{.......... (6)}\\
\end{array}$$


====================================
type: section

# Unit 6: Basic laws of Math


Associative Law
================================

$$\large\begin{eqnarray}
(a + b) + c & = & a + (b + c)\\
(1 + 3) + 4 & = & 1 + (3 + 4)\\
4 + 4 & = & 1 + 7\\
& & \\
(ab)c &=& a(bc)\\
(2\times 3) \times 5 & = & 2 \times (3 \times 5)\\
6 \times 5 & = & 2 \times 15\\
\end{eqnarray}$$

Communicative Law
================================

**Addition**

$$\large a + b = b + a$$

$$\large 2 + 3 = 3 + 2$$

***

**Multiplication**

$$\large 2 \times 3 = 3 \times 2$$

$$\large a \times b = b \times a$$

  
Distributive Law
=========================
left: 60%

**Numerical**
$$\large\begin{eqnarray}
3(5 - 2) & = & 3\times 5 - 3\times 2\\
3\times 3 & = & 15 - 6\\
9 &=& 9\\
 & & \\
 2(3 + 5) & = & 2\times 3 + 2\times 5\\
 2\times 8 &=& 6 + 10\\
 16 &=& 16 \\
 \end{eqnarray}$$

***

**Algebratic**
$$\large a(b - c) = ab - ac$$

$$\large a(b + c) = ab + ac$$



Producing a trail mix
==========================
![Trail Mix](img/TrailMix.png)

***
![Trail Mix](img/TrailMix2.png)


Recipe
====================
<tiny>

|Volume | Ingredient  | Cost / 250 gm |     
|----|------|------| 
|100 gm |roasted almonds | $4.00 |    
|100 gm |roasted pecans  | $6.50 |  
|100 gm |roasted cashews | $4.75 |    
|100 gm |sunflower seeds | $0.40 |   
|100 gm |pumpkins seeds  | $0.75 |  
|150 gm |dried cherries  | $3.90 | 
|200 gm |raisin | $5.60 |
|100 gm |cranberries | $6.90 |
|200 gm |chocolate chips | $3.50|   
|10 gm |sea salt |$0.35 |   
| 5 gm |cinnamon | $8.00 |   
| 5 gm |grown nutmeg | $9.50 |

</tiny>


Challenge Questions
====================

* What is the cost per kilo?  ............ (1)

* If the market value is US$25.00 per 500 gm, what is the profit margin?  ............ (2)


Pound Cake
====================================
$$\large\begin{array}{lrr}
Ingredient & Amt kg & Cost/kg\\
Butter & 1	& 2.5	\\			
Eggs & 1 &	3.6	\\			
Flour & 1	& 1.5	\\			
Milk & 1 &	3.8	\\			
Sugar & 1 &	1.8	\\
\hfill Totals & 5 kg & 13.2 \pounds\\
\hfill Cost/kg & & 2.64 \pounds\\
\end{array}$$


# PREPARATION FOR NEXT TIME
==============================

* **Basic algebraic concepts**
* Simplification of fractions
* Implication of equations
* Solving equations
* Read Chapter 1  
