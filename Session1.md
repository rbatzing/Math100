Fundamental Math
========================================================
title: Lecture notes: Robert Batzinger
author: IT100 Session 1: Orientation to the Course
date: 31 Aug 2017
autosize: true
css: mathclass.css
font-import: http://fonts.google.com/css?family=Bree Serif
font-family: 'Bree Serif'


![Payap Logo](img/payaplogo4.png)     
Payap University International College    
Department of Information Technology

===========================
type: section

## REGISTRATION AND INTRODUCTIONS

Registration
================

* Login to arkfriends.moodlecloud.com
  * User name: Your student ID
  * Password: it100
* Update your password

* Click on IT100/IIT100
* Answer the questions for Assign 1

* Let me know if you cannot log into this website.

Who are you?
===========================
* What is your name?
* Where are you from?
* How would you like to be called?
* Why are you here?
* What is your career plan?

Facilitator
===========================

* **Name:** Dr Robert Batzinger
* **Email:** robert_b@payap.ac.th
* **URL:** http://science.payap.ac.th/cs/bob
* **Office hours:** Wed 1-3PM, or by appointment

Where have I been for 4 weeks?
================================

* Attending to some family business in Singapore
* Studying math instruction
* Preparing for this course

***

![Dawn](img/dawn8.jpg)



===========================
type: section

## ATTITUDES TOWARDS MATH


What are your feelings about MATH?
===============================

     
* Use your computer or your mobile device.
* Go to the webpage:     
http://www.menti.com
* Enter the code:     
**15 06 32**
* Answer the questions

***

![QR Code](img/QRcode.png)

What is your experience with MATH?
===========================================

     
* Enter your vote


Question
===========================
<font size=10em>
**Who    
&nbsp;&nbsp;should be     
&nbsp;&nbsp;&nbsp;&nbsp;the instructor    
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;for this    
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;course?**
</font>

***
<font size=20em></font>

===========================
type: section

## CLASS GOALS

Your Goals
=====================

* Why are you taking this class?
* How will you use Math in your career?
* What do you want to get from this course?


Desireable milestones 
=========================
* Understand and apply mathematical transformations of equations
* Able to apply basic algebraic steps to solve a set of equations
* Able to sketch and describe key features of an equation.
* Able to identify and describe properties of common graphs and networks.
* Able to use principles learned to solve practical, real-world problems.


Course Outline
==================
<font size=6>
* Overview of the course (1)
* Integer, real number system and coordinates (1)
* Arithmetic operations and properties (1)
* Simple arithmetic operations for vectors and scalars (1) <large>**&#x25C4;**</large>A<large>**&#x25BA;**</large>
* Algebraic expressions and equations (2)
* Properties and functions of systems of equations (4) <large>**&#x25C4;**</large>A<large>**&#x25BA;**</large>
* Representation and properties of graphs and networks (2) <large>**&#x25C4;**</large>A<large>**&#x25BA;**</large>
* Systems of linear equations.(3)<large>**&#x25C4;**</large>
A<large>**&#x25BA;**</large></font>

===========================
type: section

## CLASS RULES AND MECHANICS

To attain the goals
=====================================
* What do we have to do?
* How commited are you to doing it?


Course Characteristics
=======================
* Open Textbook
* Preparation required before class
* Group work in class
* Assessment:
  * Daily activity points
  * Unit assessments: peer appraisal, contest, quiz
  * Formal Exams: Midterm / Final
  
Peer-Assessments
====================
* Value of contribution to the group effort
* Preparation for the class
* Ability to learn and to teach
* Punctuality and accuracy
* Team-worthiness


Textbooks form OpenStax.org
=================
left:15%

![Pre-Algebra](https://d3bxy9euw4e147.cloudfront.net/oscms-prodcms/media/documents/prealgebra_1.svg)

![College Algebra](https://d3bxy9euw4e147.cloudfront.net/oscms-prodcms/media/documents/college_algebra.svg)

![Pre-Calculus](https://d3bxy9euw4e147.cloudfront.net/oscms-prodcms/media/documents/precalculus.svg)

***
* **[Pre-Algebra](https://d3bxy9euw4e147.cloudfront.net/oscms-prodcms/media/documents/Prealgebra-OP.pdf)**       
Lynn Marecek and MaryAnne Anthony-Smith (2015).    
ISBN: 978-1-938168-99-4     

* **[College Algebra](https://d3bxy9euw4e147.cloudfront.net/oscms-prodcms/media/documents/CollegeAlgebra-OP.pdf).**      
Jay Abramson (2015).     
ISBN: 978-1-938168-38-3     


* **[Precalculus](https://d3bxy9euw4e147.cloudfront.net/oscms-prodcms/media/documents/Precalculus-OP.pdf).** ISBN: 978-1-938168-34-5     
Jay Abramson (2014).     



=================
type: section

# WORKING IN TEAMS


Belkin Roles
===================
"A Team Role is a tendency to behave, contribute and interrelate with others in a particular way"
Dr Meredith Belbin

Building a Team
=====================

* Successful teams need a mix of people that will cover all critical team role behaviours

* The key is to understand the strengths and allowable weaknesses for each of the roles

* On smaller teams, members cover the three team roles that they are most comfortable with.


Categories of Team Roles
========================

* **Action-oriented Roles:**     
Shaper, Implementer, Completer Finisher
* **People-oriented Roles:**     
Co-ordinator, Teamworker, Resource Investigator
* **Thinking-oriented Roles:**     
Plant, Monitor Evaluator, Specialist


Resource Investigator
======================
* <large>**&#x25BA;**</large> Uses their inquisitive nature to find ideas to bring back to the team. 

* <large>**&#x2714;**</large> Outgoing, enthusiastic. Explores opportunities and develops contacts.

* <large>**&#x2716;**</large> Might be over-optimistic, and can lose interest once the initial enthusiasm has passed.

* <large>&#x261B;</large> They might forget to follow up on a lead.


Teamworker
=========================
* <large>**&#x25BA;**</large> Helps the team to gel, using their versatility to identify the work required and complete it on behalf of the team.

* <large>**&#x2714;**</large> Co-operative, perceptive and diplomatic. Listens and averts friction.

* <large>**&#x2716;**</large> Can be indecisive in crunch situations and tends to avoid confrontation.

* <large>&#x261B;</large> They might be hesitant to make unpopular decisions.


Co-ordinator
============================
* <large>**&#x25BA;**</large> Needed to focus on the team's objectives, draw out team members and delegate work appropriately.

* <large>**&#x2714;**</large> Mature, confident, identifies talent. Clarifies goals.

* <large>**&#x2716;**</large> Can be seen as manipulative and might offload their own share of the work.

* <large>&#x261B;</large> They might over-delegate, leaving themselves little work to do.


Plant
================================
* <large>**&#x25BA;**</large> Tends to be highly creative and good at solving problems in unconventional ways.

* <large>**&#x2714;**</large> Creative, imaginative, free-thinking, generates ideas and solves difficult problems.

* <large>**&#x2716;**</large> Might ignore incidentals, and may be too preoccupied to communicate effectively.

* <large>&#x261B;</large> They could be absent-minded or forgetful.


Monitor Evaluator
================================
* <large>**&#x25BA;**</large> Provides a logical eye, making impartial judgements where required and weighs up the team's options in a dispassionate way.

* <large>**&#x2714;**</large> Sober, strategic and discerning. Sees all options and judges accurately.

* <large>**&#x2716;**</large> Sometimes lacks the drive and ability to inspire others and can be overly critical.

* <large>&#x261B;</large> They could be slow to come to decisions.


Specialist
===========================
* <large>**&#x25BA;**</large> Brings in-depth knowledge of a key area to the team.

* <large>**&#x2714;**</large> Single-minded, self-starting and dedicated. They provide specialist knowledge and skills.

* <large>**&#x2716;**</large> Tends to contribute on a narrow front and can dwell on the technicalities.

* <large>&#x261B;</large> They overload you with information.


Shaper
=============================
* <large>**&#x25BA;**</large> Provides the necessary drive to ensure that the team keeps moving and does not lose focus or momentum.

* <large>**&#x2714;**</large> Challenging, dynamic, thrives on pressure. Has the drive and courage to overcome obstacles.

* <large>**&#x2716;**</large> Can be prone to provocation, and may sometimes offend people's feelings.

* <large>&#x261B;</large> They could risk becoming aggressive and bad-humoured in their attempts to get things done.


Implementer
==============================
* <large>**&#x25BA;**</large> Needed to plan a workable strategy and carry it out as efficiently as possible.

* <large>**&#x2714;**</large> Practical, reliable, efficient. Turns ideas into actions and organises work to be done.

* <large>**&#x2716;**</large> Can be inflexible and slow to respond to new possibilities.

* <large>&#x261B;</large> They can be slow to relinquish their plans in favour of positive changes.


Completer Finisher
==================================
* <large>**&#x25BA;**</large> Most effectively used at the end of tasks to polish and scrutinise the work for errors, subjecting it to the highest standards of quality control.

* <large>**&#x2714;**</large> Painstaking, conscientious, anxious. Searches out errors. Polishes and perfects.

* <large>**&#x2716;**</large> Can be inclined to worry unduly, and reluctant to delegate.

* <large>&#x261B;</large> They could be accused of taking their perfectionism to extremes.


Team Roles required at different phases
============================

| **Project phase** | **Roles required**  |
|------|----------------------------|
| Identify goals | Shaper, Co-ordinator |
| Ideas | Plant, Resource investigator |
| Plans | Monitor evaluator, Specialist |
| Contacts | Resource investigator, Teamworker |
| Organisation | Implementer, Co-ordinator |
| Follow through | Completer finisher, Implementer |



===========================
type: section

